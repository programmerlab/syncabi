webpackJsonp(["app-inbox.module"],{

/***/ "../../../../../src/app/views/app-inbox/app-inbox.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/app-inbox/app-inbox.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container>\r\n  <!-- Inbox left side bar -->\r\n  <mat-sidenav #sidenav [opened]=\"isSidenavOpen\" mode=\"side\" class=\"inbox-sidenav\">\r\n    <!-- Compose button -->\r\n    <button mat-raised-button class=\"mat-warn full-width\" (click)=\"openComposeDialog()\">Compose</button>\r\n    <!-- left side buttons -->\r\n    <mat-nav-list class=\"inbox-nav-list\" role=\"list\">\r\n      <mat-list-item class=\"primary-imenu-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <a fxLayout=\"row\">\r\n          <mat-icon class=\"text-muted\">inbox</mat-icon>\r\n          <span>Inbox</span>\r\n        </a>\r\n      </mat-list-item>\r\n      <mat-list-item class=\"primary-imenu-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <a fxLayout=\"row\">\r\n          <mat-icon class=\"text-muted\">folder_special</mat-icon>\r\n          <span>Starred</span>\r\n        </a>\r\n      </mat-list-item>\r\n      <mat-list-item class=\"primary-imenu-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <a fxLayout=\"row\">\r\n          <mat-icon class=\"text-muted\">send</mat-icon>\r\n          <span>Sent</span>\r\n        </a>\r\n      </mat-list-item>\r\n      <mat-list-item class=\"primary-imenu-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <a fxLayout=\"row\">\r\n          <mat-icon class=\"text-muted\">drafts</mat-icon>\r\n          <span>Draft</span>\r\n        </a>\r\n      </mat-list-item>\r\n      <mat-list-item class=\"primary-imenu-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <a fxLayout=\"row\">\r\n          <mat-icon class=\"text-muted\">error</mat-icon>\r\n          <span>Spam</span>\r\n        </a>\r\n      </mat-list-item>\r\n\r\n      <mat-divider></mat-divider>\r\n\r\n      <mat-list-item class=\"\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <a fxLayout=\"row\">\r\n          <mat-icon color=\"primary\">people</mat-icon>\r\n          <span>Social</span>\r\n        </a>\r\n      </mat-list-item>\r\n      <mat-list-item class=\"\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <a fxLayout=\"row\">\r\n          <mat-icon color=\"warn\">local_offer</mat-icon>\r\n          <span>Promotions</span>\r\n        </a>\r\n      </mat-list-item>\r\n      <mat-list-item class=\"\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <a fxLayout=\"row\">\r\n          <mat-icon color=\"accent\">forums</mat-icon>\r\n          <span>Forums</span>\r\n        </a>\r\n      </mat-list-item>\r\n    </mat-nav-list>\r\n  </mat-sidenav>\r\n\r\n  <div class=\"messages-wrap\">\r\n    <!-- right side topbar -->\r\n    <mat-toolbar color=\"primary\" class=\"inbox-toolbar\">\r\n      <!-- Left sidebar toggle button -->\r\n      <button mat-icon-button (click)=\"sidenav.toggle()\" fxShow [fxShow.gt-sm]=\"isMobile\"><mat-icon>short_text</mat-icon></button>\r\n      <!-- multiple email select and control -->\r\n      <mat-checkbox (change)=\"selectToggleAll()\" class=\"inbox-toggle-all mr-1\"><small>All</small></mat-checkbox>\r\n      <button mat-icon-button matTooltip=\"Move to trash\"><mat-icon>delete</mat-icon></button>\r\n      <button mat-icon-button matTooltip=\"Mark as important\" class=\"\"><mat-icon>folder_special</mat-icon></button>\r\n      <button mat-icon-button matTooltip=\"Move to archive\"><mat-icon>archive</mat-icon></button>\r\n      <button mat-icon-button matTooltip=\"Report spam\"><mat-icon>error</mat-icon></button>\r\n    </mat-toolbar>\r\n\r\n    <div class=\"inbox-hold\">\r\n      <mat-list class=\"messages\">\r\n        <mat-list-item *ngFor=\"let message of messages\" class=\"message-item app-accordion\">\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" class=\"message-head\">\r\n            <!-- Single email select and star (show on close) -->\r\n            <mat-checkbox [(ngModel)]=\"message.selected\" class=\"hidden-on-open mail-check\"></mat-checkbox>\r\n            <button \r\n            mat-icon-button \r\n            matTooltip=\"Mark as important\" \r\n            class=\"hidden-on-open text-muted mr-1\"><mat-icon>star_border</mat-icon></button>\r\n            <!-- Single emial photo and date (show on open) -->\r\n            <div fxFlex=\"1 1 auto\" fxLayout=\"row\" class=\"show-on-open mr-1\">\r\n              <img [src]=\"message.sender.photo\" alt=\"\" class=\"inbox-face mr-1\">\r\n              <div fxLayout=\"column\">\r\n                <span class=\"mat-card-title m-0\">{{message.sender.name}}</span>\r\n                <small class=\"mat-card-subtitle m-0\">{{message.date | date}}</small>\r\n              </div>\r\n            </div>\r\n            <!-- Single emial name and subject -->\r\n            <div appAccordion fxFlex=\"18 1 auto\" fxLayout=\"row\">\r\n              <!-- (show on close) -->\r\n              <div class=\"mr-1 hidden-on-open\" [ngStyle]=\"{minWidth: '120px'}\">{{message.sender.name}}</div>\r\n              <!-- (always show) -->\r\n              <div>{{message.subject}}</div>\r\n            </div>\r\n            <span fxFlex fxHide [fxHide.gt-sm]=\"isMobile\"></span>\r\n            <!-- Single emial date (show on close) -->\r\n            <small class=\"hidden-on-open\">{{message.date | date}}</small>\r\n            <!-- Single emial top Drop down (show on close) -->\r\n            <button mat-icon-button [matMenuTriggerFor]=\"msgMenu\" class=\"hidden-on-open\">\r\n                <mat-icon class=\"text-muted\">more_vert</mat-icon>\r\n            </button>\r\n            <mat-menu #msgMenu=\"matMenu\">\r\n              <button mat-menu-item>Reply</button>\r\n              <button mat-menu-item>Archive</button>\r\n              <button mat-menu-item>Delete</button>\r\n            </mat-menu>\r\n\r\n            <!-- Single emial top Icons (show on open) -->\r\n            <div class=\"show-on-open\">\r\n              <button mat-icon-button matTooltip=\"Reply\"><mat-icon class=\"text-muted\">reply</mat-icon></button>\r\n              <button mat-icon-button matTooltip=\"Mark as important\"><mat-icon class=\"text-muted\">folder_special</mat-icon></button>\r\n              <button mat-icon-button matTooltip=\"Move to archive\"><mat-icon class=\"text-muted\">archive</mat-icon></button>\r\n              <button mat-icon-button matTooltip=\"Move to trash\"><mat-icon class=\"text-muted\">delete</mat-icon></button>\r\n            </div>\r\n          </div>\r\n\r\n          <!-- Single text (show on open) -->\r\n          <div class=\"message-content accordion-content\">\r\n            <div class=\"message-text\" [innerHTML]=\"message.message\"></div>\r\n          </div>\r\n        </mat-list-item>\r\n      </mat-list>\r\n    </div>\r\n\r\n  </div>\r\n\r\n</mat-sidenav-container>"

/***/ }),

/***/ "../../../../../src/app/views/app-inbox/app-inbox.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppInboxComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__ = __webpack_require__("../../../flex-layout/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_inbox_service__ = __webpack_require__("../../../../../src/app/views/app-inbox/app-inbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mail_compose_component__ = __webpack_require__("../../../../../src/app/views/app-inbox/mail-compose.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppInboxComponent = (function () {
    function AppInboxComponent(router, media, composeDialog, inboxService) {
        this.router = router;
        this.media = media;
        this.composeDialog = composeDialog;
        this.inboxService = inboxService;
        this.isSidenavOpen = true;
        this.selectToggleFlag = false;
    }
    AppInboxComponent.prototype.ngOnInit = function () {
        this.inboxSideNavInit();
        this.messages = this.inboxService.messages;
    };
    AppInboxComponent.prototype.openComposeDialog = function () {
        var dialogRef = this.composeDialog.open(__WEBPACK_IMPORTED_MODULE_5__mail_compose_component__["a" /* MailComposeComponent */]);
        dialogRef.afterClosed().subscribe(function (result) { });
    };
    AppInboxComponent.prototype.selectToggleAll = function () {
        var _this = this;
        this.selectToggleFlag = !this.selectToggleFlag;
        this.messages.forEach(function (msg) { msg.selected = _this.selectToggleFlag; });
    };
    AppInboxComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNave.mode = self.isMobile ? 'over' : 'side';
        });
    };
    AppInboxComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    return AppInboxComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3__angular_material__["A" /* MatSidenav */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_material__["A" /* MatSidenav */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_material__["A" /* MatSidenav */]) === "function" && _a || Object)
], AppInboxComponent.prototype, "sideNave", void 0);
AppInboxComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-inbox',
        template: __webpack_require__("../../../../../src/app/views/app-inbox/app-inbox.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/app-inbox/app-inbox.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_4__app_inbox_service__["a" /* AppInboxService */]]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["b" /* ObservableMedia */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["b" /* ObservableMedia */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_material__["i" /* MatDialog */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_material__["i" /* MatDialog */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__app_inbox_service__["a" /* AppInboxService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__app_inbox_service__["a" /* AppInboxService */]) === "function" && _e || Object])
], AppInboxComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=app-inbox.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/app-inbox/app-inbox.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppInboxModule", function() { return AppInboxModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__ = __webpack_require__("../../../flex-layout/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_quill__ = __webpack_require__("../../../../ngx-quill/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__directives_common_common_directives_module__ = __webpack_require__("../../../../../src/app/directives/common/common-directives.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_inbox_component__ = __webpack_require__("../../../../../src/app/views/app-inbox/app-inbox.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__mail_compose_component__ = __webpack_require__("../../../../../src/app/views/app-inbox/mail-compose.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_inbox_routing__ = __webpack_require__("../../../../../src/app/views/app-inbox/app-inbox.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppInboxModule = (function () {
    function AppInboxModule() {
    }
    return AppInboxModule;
}());
AppInboxModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["n" /* MatIconModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["c" /* MatButtonModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["J" /* MatToolbarModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["B" /* MatSidenavModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["q" /* MatMenuModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["o" /* MatInputModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["j" /* MatDialogModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["p" /* MatListModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MatCheckboxModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["K" /* MatTooltipModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__["a" /* FlexLayoutModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_quill__["a" /* QuillModule */],
            __WEBPACK_IMPORTED_MODULE_7__directives_common_common_directives_module__["a" /* CommonDirectivesModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_10__app_inbox_routing__["a" /* InboxRoutes */])
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_8__app_inbox_component__["a" /* AppInboxComponent */], __WEBPACK_IMPORTED_MODULE_9__mail_compose_component__["a" /* MailComposeComponent */]],
        entryComponents: [__WEBPACK_IMPORTED_MODULE_9__mail_compose_component__["a" /* MailComposeComponent */]]
    })
], AppInboxModule);

//# sourceMappingURL=app-inbox.module.js.map

/***/ }),

/***/ "../../../../../src/app/views/app-inbox/app-inbox.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InboxRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_inbox_component__ = __webpack_require__("../../../../../src/app/views/app-inbox/app-inbox.component.ts");

var InboxRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_0__app_inbox_component__["a" /* AppInboxComponent */], data: { title: 'Inbox' } }
];
//# sourceMappingURL=app-inbox.routing.js.map

/***/ }),

/***/ "../../../../../src/app/views/app-inbox/app-inbox.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppInboxService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppInboxService = (function () {
    function AppInboxService() {
        this.messages = [
            {
                sender: {
                    name: 'Henrik Gevorg',
                    photo: 'assets/images/face-1.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'Welcome to Angular Egret',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p><br>\n            Thanks<br>\n            Jhone"
            },
            {
                sender: {
                    name: 'Gevorg Spartak',
                    photo: 'assets/images/face-2.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'Confirm your email address',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote><br>\n            Thanks<br>\n            Mark"
            },
            {
                sender: {
                    name: 'Petros Toros',
                    photo: 'assets/images/face-3.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'New order informations',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p><br>\n            Thanks<br>\n            Jhone"
            },
            {
                sender: {
                    name: 'Henrik Gevorg',
                    photo: 'assets/images/face-1.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'Welcome to Angular Egret',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p><br>\n            Thanks<br>\n            Jhone"
            },
            {
                sender: {
                    name: 'Gevorg Spartak',
                    photo: 'assets/images/face-2.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'Confirm your email address',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote><br>\n            Thanks<br>\n            Mark"
            },
            {
                sender: {
                    name: 'Petros Toros',
                    photo: 'assets/images/face-4.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'New order informations',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p><br>\n            Thanks<br>\n            Jhone"
            },
            {
                sender: {
                    name: 'Henrik Gevorg',
                    photo: 'assets/images/face-1.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'Welcome to Angular Egret',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p><br>\n            Thanks<br>\n            Jhone"
            },
            {
                sender: {
                    name: 'Gevorg Spartak',
                    photo: 'assets/images/face-2.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'Confirm your email address',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote><br>\n            Thanks<br>\n            Mark"
            },
            {
                sender: {
                    name: 'Petros Toros',
                    photo: 'assets/images/face-4.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'New order informations',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p><br>\n            Thanks<br>\n            Jhone"
            },
            {
                sender: {
                    name: 'Gevorg Spartak',
                    photo: 'assets/images/face-2.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'Confirm your email address',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote><br>\n            Thanks<br>\n            Mark"
            },
            {
                sender: {
                    name: 'Petros Toros',
                    photo: 'assets/images/face-4.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'New order informations',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p><br>\n            Thanks<br>\n            Jhone"
            },
            {
                sender: {
                    name: 'Gevorg Spartak',
                    photo: 'assets/images/face-2.jpg'
                },
                date: new Date('10/3/2015'),
                selected: false,
                subject: 'Confirm your email address',
                message: "<p>Natus consequuntur perspiciatis esse beatae illo quos eaque.</p>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote>\n            <p>Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi. Iusto ipsam, nihil? Eveniet modi maxime animi excepturi a dignissimos doloribus, \n            inventore sed ratione, ducimus atque earum maiores tenetur officia commodi dicta tempora consequatur non nesciunt ipsam, \n            consequuntur quia fuga aspernatur impedit et? Natus, earum.</p>\n            <blockquote>\n            Earum, quisquam, fugit? Numquam dolor magni nisi? Suscipit odit, ipsam iusto enim culpa, \n            temporibus vero possimus error voluptates sequi.\n            </blockquote><br>\n            Thanks<br>\n            Mark"
            }
        ];
    }
    return AppInboxService;
}());
AppInboxService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], AppInboxService);

//# sourceMappingURL=app-inbox.service.js.map

/***/ }),

/***/ "../../../../../src/app/views/app-inbox/mail-compose.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MailComposeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MailComposeComponent = (function () {
    function MailComposeComponent(composeDialog) {
        this.composeDialog = composeDialog;
        this.newMailData = {};
    }
    MailComposeComponent.prototype.ngOnInit = function () {
        this.mailForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormGroup"]({
            to: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email
            ]),
            subject: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
            ]),
            message: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
            ])
        });
    };
    MailComposeComponent.prototype.sendEmail = function () {
        console.log(this.mailForm.value);
    };
    MailComposeComponent.prototype.closeDialog = function () {
    };
    return MailComposeComponent;
}());
MailComposeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'mail-compose',
        template: __webpack_require__("../../../../../src/app/views/app-inbox/mail-compose.template.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatDialog */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatDialog */]) === "function" && _a || Object])
], MailComposeComponent);

var _a;
//# sourceMappingURL=mail-compose.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/app-inbox/mail-compose.template.html":
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"mailForm\" novalidate (submit)=\"sendEmail()\">\r\n    <div class=\"pb-1\">\r\n        <mat-input-container class=\"full-width\">\r\n            <input\r\n            matInput\r\n            formControlName=\"to\"\r\n            placeholder=\"To\">\r\n        </mat-input-container>\r\n        <small \r\n        *ngIf=\"mailForm.controls.to.errors && (mailForm.controls.to.dirty || mailForm.controls.to.touched) && (mailForm.controls.to.errors.required)\" \r\n        class=\"form-error-msg\"> Email is required </small>\r\n        <small \r\n        *ngIf=\"mailForm.controls.to.errors && (mailForm.controls.to.dirty || mailForm.controls.to.touched) && (mailForm.controls.to.errors.email)\" \r\n        class=\"form-error-msg\"> Invaild email address </small>\r\n    </div>\r\n\r\n    <div class=\"pb-1\">\r\n        <mat-input-container class=\"full-width\">\r\n            <input\r\n            matInput\r\n            name=\"subject\"\r\n            formControlName=\"subject\"\r\n            placeholder=\"Subject\"\r\n            value=\"\">\r\n        </mat-input-container>\r\n        <small \r\n        *ngIf=\"mailForm.controls.subject.errors && \r\n        (mailForm.controls.subject.dirty || mailForm.controls.subject.touched) && \r\n        (mailForm.controls.subject.errors.required)\" \r\n        class=\"form-error-msg\"> Subject is required </small>\r\n    </div>\r\n    <div class=\"pb-1\">\r\n        <quill-editor theme=\"snow\" formControlName=\"message\"></quill-editor>\r\n        <small \r\n        *ngIf=\"mailForm.controls.message.errors && \r\n        (mailForm.controls.message.dirty || mailForm.controls.message.touched) && \r\n        (mailForm.controls.message.errors.required)\" \r\n        class=\"form-error-msg\"> Message is required </small>\r\n    </div>\r\n\r\n    <div class=\"\">\r\n        <button\r\n        type=\"button\"\r\n        mat-button \r\n        mat-dialog-close>Cancel</button>\r\n        <span fxFlex></span>\r\n        <button mat-icon-button><mat-icon>attachment</mat-icon></button>\r\n        <button mat-mini-fab class=\"mat-primary\"><mat-icon>send</mat-icon></button>\r\n    </div>\r\n</form>"

/***/ })

});
//# sourceMappingURL=app-inbox.module.chunk.js.map