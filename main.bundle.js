webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./views/app-calendar/app-calendar.module": [
		"../../../../../src/app/views/app-calendar/app-calendar.module.ts",
		"app-calendar.module"
	],
	"./views/app-chats/app-chats.module": [
		"../../../../../src/app/views/app-chats/app-chats.module.ts",
		"app-chats.module"
	],
	"./views/app-dialogs/app-dialogs.module": [
		"../../../../../src/app/views/app-dialogs/app-dialogs.module.ts",
		"app-dialogs.module"
	],
	"./views/app-inbox/app-inbox.module": [
		"../../../../../src/app/views/app-inbox/app-inbox.module.ts",
		"common",
		"app-inbox.module"
	],
	"./views/app-tour/app-tour.module": [
		"../../../../../src/app/views/app-tour/app-tour.module.ts",
		"app-tour.module"
	],
	"./views/charts/charts.module": [
		"../../../../../src/app/views/charts/charts.module.ts",
		"common",
		"charts.module"
	],
	"./views/dashboard/dashboard.module": [
		"../../../../../src/app/views/dashboard/dashboard.module.ts",
		"common",
		"dashboard.module"
	],
	"./views/dragndrop/dragndrop.module": [
		"../../../../../src/app/views/dragndrop/dragndrop.module.ts",
		"dragndrop.module"
	],
	"./views/forms/forms.module": [
		"../../../../../src/app/views/forms/forms.module.ts",
		"forms.module",
		"common"
	],
	"./views/map/map.module": [
		"../../../../../src/app/views/map/map.module.ts",
		"map.module"
	],
	"./views/mat-icons/mat-icons.module": [
		"../../../../../src/app/views/mat-icons/mat-icons.module.ts",
		"mat-icons.module"
	],
	"./views/material/app-material.module": [
		"../../../../../src/app/views/material/app-material.module.ts",
		"app-material.module"
	],
	"./views/others/others.module": [
		"../../../../../src/app/views/others/others.module.ts",
		"common",
		"others.module"
	],
	"./views/profile/profile.module": [
		"../../../../../src/app/views/profile/profile.module.ts",
		"common",
		"profile.module"
	],
	"./views/sessions/sessions.module": [
		"../../../../../src/app/views/sessions/sessions.module.ts",
		"sessions.module"
	],
	"./views/tables/tables.module": [
		"../../../../../src/app/views/tables/tables.module.ts",
		"common",
		"tables.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hopscotch__ = __webpack_require__("../../../../hopscotch/dist/js/hopscotch.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hopscotch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_hopscotch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_route_parts_route_parts_service__ = __webpack_require__("../../../../../src/app/services/route-parts/route-parts.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppComponent = (function () {
    function AppComponent(title, router, activeRoute, routePartsService, snackBar) {
        this.title = title;
        this.router = router;
        this.activeRoute = activeRoute;
        this.routePartsService = routePartsService;
        this.snackBar = snackBar;
        this.appTitle = 'Egret';
        this.pageTitle = '';
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.changePageTitle();
        // Init User Tour
        setTimeout(function () {
            __WEBPACK_IMPORTED_MODULE_4_hopscotch__["startTour"](_this.tourSteps());
        }, 2000);
    };
    /*
    ***** Tour Steps ****
    * You can supply tourSteps directly in hopscotch.startTour instead of
    * returning value by invoking tourSteps method,
    * but DOM query methods(querySelector, getElementsByTagName etc) will not work
    */
    AppComponent.prototype.tourSteps = function () {
        var self = this;
        return {
            id: 'hello-egret',
            showPrevButton: true,
            onEnd: function () {
                self.snackBar.open('Awesome! Now let\'s explore Egret\'s cool features.', 'close', { duration: 5000 });
            },
            onClose: function () {
                self.snackBar.open('You just closed User Tour!', 'close', { duration: 3000 });
            },
            steps: [
                {
                    title: 'Sidebar Controls',
                    content: 'Control left sidebar\'s display style.',
                    target: 'sidenavToggle',
                    placement: 'bottom',
                    xOffset: 10
                },
                {
                    title: 'Available Themes',
                    content: 'Choose a color scheme.',
                    target: 'schemeToggle',
                    placement: 'left',
                    xOffset: 20
                },
                {
                    title: 'Language',
                    content: 'Choose your language.',
                    target: document.querySelector('.topbar .mat-select'),
                    placement: 'left',
                    xOffset: 10,
                    yOffset: -5
                }
            ]
        };
    };
    AppComponent.prototype.changePageTitle = function () {
        var _this = this;
        this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (routeChange) {
            var routeParts = _this.routePartsService.generateRouteParts(_this.activeRoute.snapshot);
            if (!routeParts.length)
                return _this.title.setTitle(_this.appTitle);
            // Extract title from parts;
            _this.pageTitle = routeParts
                .reverse()
                .map(function (part) { return part.title; })
                .reduce(function (partA, partI) { return partA + " > " + partI; });
            _this.pageTitle += " | " + _this.appTitle;
            _this.title.setTitle(_this.pageTitle);
        });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__services_route_parts_route_parts_service__["a" /* RoutePartsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_route_parts_route_parts_service__["a" /* RoutePartsService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_material__["E" /* MatSnackBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_material__["E" /* MatSnackBar */]) === "function" && _e || Object])
], AppComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/@angular/platform-browser/animations.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_translate_ng2_translate__ = __webpack_require__("../../../../ng2-translate/ng2-translate.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_routes__ = __webpack_require__("../../../../../src/app/app.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_common_app_common_module__ = __webpack_require__("../../../../../src/app/components/common/app-common.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_route_parts_route_parts_service__ = __webpack_require__("../../../../../src/app/services/route-parts/route-parts.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_navigation_navigation_service__ = __webpack_require__("../../../../../src/app/services/navigation/navigation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_auth_auth_service__ = __webpack_require__("../../../../../src/app/services/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_5_ng2_translate_ng2_translate__["d" /* TranslateStaticLoader */](http, './assets/i18n', '.json');
}
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_7__components_common_app_common_module__["a" /* AppCommonModule */],
            __WEBPACK_IMPORTED_MODULE_5_ng2_translate_ng2_translate__["b" /* TranslateModule */].forRoot({
                provide: __WEBPACK_IMPORTED_MODULE_5_ng2_translate_ng2_translate__["a" /* TranslateLoader */],
                useFactory: (createTranslateLoader),
                deps: [__WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */]]
            }),
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_routes__["a" /* rootRouterConfig */], { useHash: false })
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_9__services_route_parts_route_parts_service__["a" /* RoutePartsService */], __WEBPACK_IMPORTED_MODULE_10__services_navigation_navigation_service__["a" /* NavigationService */], __WEBPACK_IMPORTED_MODULE_11__services_auth_auth_service__["a" /* AuthService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return rootRouterConfig; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_common_layouts_admin_layout_admin_layout_component__ = __webpack_require__("../../../../../src/app/components/common/layouts/admin-layout/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_common_layouts_auth_layout_auth_layout_component__ = __webpack_require__("../../../../../src/app/components/common/layouts/auth-layout/auth-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_auth_service__ = __webpack_require__("../../../../../src/app/services/auth/auth.service.ts");



var rootRouterConfig = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_1__components_common_layouts_auth_layout_auth_layout_component__["a" /* AuthLayoutComponent */],
        children: [
            {
                path: '',
                loadChildren: './views/sessions/sessions.module#SessionsModule',
                data: { title: 'Session' }
            }
        ]
    },
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__components_common_layouts_admin_layout_admin_layout_component__["a" /* AdminLayoutComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_2__services_auth_auth_service__["a" /* AuthService */]],
        children: [
            {
                path: 'dashboard',
                loadChildren: './views/dashboard/dashboard.module#DashboardModule',
                data: { title: 'Dashboard', breadcrumb: 'DASHBOARD' }
            },
            {
                path: 'material',
                loadChildren: './views/material/app-material.module#AppMaterialModule',
                data: { title: 'Material', breadcrumb: 'MATERIAL' }
            },
            {
                path: 'dialogs',
                loadChildren: './views/app-dialogs/app-dialogs.module#AppDialogsModule',
                data: { title: 'Dialogs', breadcrumb: 'DIALOGS' }
            },
            {
                path: 'profile',
                loadChildren: './views/profile/profile.module#ProfileModule',
                data: { title: 'Profile', breadcrumb: 'PROFILE' }
            },
            {
                path: 'others',
                loadChildren: './views/others/others.module#OthersModule',
                data: { title: 'Others', breadcrumb: 'OTHERS' }
            },
            {
                path: 'tables',
                loadChildren: './views/tables/tables.module#TablesModule',
                data: { title: 'Tables', breadcrumb: 'TABLES' }
            },
            {
                path: 'tour',
                loadChildren: './views/app-tour/app-tour.module#AppTourModule',
                data: { title: 'Tour', breadcrumb: 'TOUR' }
            },
            {
                path: 'forms',
                loadChildren: './views/forms/forms.module#AppFormsModule',
                data: { title: 'Forms', breadcrumb: 'FORMS' }
            },
            {
                path: 'charts',
                loadChildren: './views/charts/charts.module#AppChartsModule',
                data: { title: 'Charts', breadcrumb: 'CHARTS' }
            },
            {
                path: 'map',
                loadChildren: './views/map/map.module#AppMapModule',
                data: { title: 'Map', breadcrumb: 'MAP' }
            },
            {
                path: 'dragndrop',
                loadChildren: './views/dragndrop/dragndrop.module#DragndropModule',
                data: { title: 'Drag and Drop', breadcrumb: 'DND' }
            },
            {
                path: 'inbox',
                loadChildren: './views/app-inbox/app-inbox.module#AppInboxModule',
                data: { title: 'Inbox', breadcrumb: 'INBOX' }
            },
            {
                path: 'calendar',
                loadChildren: './views/app-calendar/app-calendar.module#AppCalendarModule',
                data: { title: 'Calendar', breadcrumb: 'CALENDAR' }
            },
            {
                path: 'chat',
                loadChildren: './views/app-chats/app-chats.module#AppChatsModule',
                data: { title: 'Chat', breadcrumb: 'CHAT' }
            },
            {
                path: 'icons',
                loadChildren: './views/mat-icons/mat-icons.module#MatIconsModule',
                data: { title: 'Icons', breadcrumb: 'MATICONS' }
            }
        ]
    },
    {
        path: '**',
        redirectTo: 'sessions/404'
    }
];
//# sourceMappingURL=app.routes.js.map

/***/ }),

/***/ "../../../../../src/app/components/common/app-common.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppCommonModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__ = __webpack_require__("../../../flex-layout/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_translate_ng2_translate__ = __webpack_require__("../../../../ng2-translate/ng2-translate.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__topbar_topbar_component__ = __webpack_require__("../../../../../src/app/components/common/topbar/topbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__navigation_navigation_component__ = __webpack_require__("../../../../../src/app/components/common/navigation/navigation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__notifications_notifications_component__ = __webpack_require__("../../../../../src/app/components/common/notifications/notifications.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__layouts_admin_layout_admin_layout_component__ = __webpack_require__("../../../../../src/app/components/common/layouts/admin-layout/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__layouts_auth_layout_auth_layout_component__ = __webpack_require__("../../../../../src/app/components/common/layouts/auth-layout/auth-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__directives_common_common_directives_module__ = __webpack_require__("../../../../../src/app/directives/common/common-directives.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_theme_theme_service__ = __webpack_require__("../../../../../src/app/services/theme/theme.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__customizer_customizer_component__ = __webpack_require__("../../../../../src/app/components/common/customizer/customizer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__breadcrumb_breadcrumb_component__ = __webpack_require__("../../../../../src/app/components/common/breadcrumb/breadcrumb.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppCommonModule = (function () {
    function AppCommonModule() {
    }
    return AppCommonModule;
}());
AppCommonModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["d" /* RouterModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__["a" /* FlexLayoutModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["B" /* MatSidenavModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["n" /* MatIconModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["p" /* MatListModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["K" /* MatTooltipModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["s" /* MatOptionModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["z" /* MatSelectModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["q" /* MatMenuModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["F" /* MatSnackBarModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["m" /* MatGridListModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["J" /* MatToolbarModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["c" /* MatButtonModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["x" /* MatRadioModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["f" /* MatCheckboxModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["e" /* MatCardModule */],
            __WEBPACK_IMPORTED_MODULE_13__directives_common_common_directives_module__["a" /* CommonDirectivesModule */],
            __WEBPACK_IMPORTED_MODULE_6_ng2_translate_ng2_translate__["b" /* TranslateModule */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_11__layouts_admin_layout_admin_layout_component__["a" /* AdminLayoutComponent */],
            __WEBPACK_IMPORTED_MODULE_12__layouts_auth_layout_auth_layout_component__["a" /* AuthLayoutComponent */],
            __WEBPACK_IMPORTED_MODULE_8__topbar_topbar_component__["a" /* TopbarComponent */],
            __WEBPACK_IMPORTED_MODULE_9__navigation_navigation_component__["a" /* NavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_10__notifications_notifications_component__["a" /* NotificationsComponent */], __WEBPACK_IMPORTED_MODULE_15__customizer_customizer_component__["a" /* CustomizerComponent */], __WEBPACK_IMPORTED_MODULE_16__breadcrumb_breadcrumb_component__["a" /* BreadcrumbComponent */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_14__services_theme_theme_service__["a" /* ThemeService */]],
        exports: []
    })
], AppCommonModule);

//# sourceMappingURL=app-common.module.js.map

/***/ }),

/***/ "../../../../../src/app/components/common/breadcrumb/breadcrumb.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".breadcrumb-bar {\r\n  width: 100%;\r\n  height: 40px !important;\r\n  min-height: 40px !important;\r\n  padding: 0 .666rem;\r\n  float: left;\r\n  box-shadow: 0 3px 1px -2px rgba(0,0,0,.08), 0 2px 2px 0 rgba(0,0,0,.05), 0 1px 5px 0 rgba(0,0,0,.05);\r\n}\r\n.breadcrumb {\r\n  margin: 0;\r\n  padding: 0;\r\n}\r\n.breadcrumb li {\r\n  list-style: none;\r\n  float: left;\r\n  line-height: 40px;\r\n}\r\n.breadcrumb li:not(:first-child):before {\r\n  content: \"/\\A0\";\r\n  padding: 0 8px;\r\n}\r\n.breadcrumb li a {\r\n  font-weight: 400;\r\n  font-size: 1rem;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/common/breadcrumb/breadcrumb.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"breadcrumb-bar\" *ngIf=\"isEnabled\">\n  <ul class=\"breadcrumb\">\n    <li *ngFor=\"let part of routeParts\"><a routerLink=\"/{{part.url}}\">{{part.breadcrumb | translate}}</a></li>\n  </ul>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/common/breadcrumb/breadcrumb.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BreadcrumbComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_route_parts_route_parts_service__ = __webpack_require__("../../../../../src/app/services/route-parts/route-parts.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BreadcrumbComponent = (function () {
    function BreadcrumbComponent(router, routePartsService, activeRoute) {
        var _this = this;
        this.router = router;
        this.routePartsService = routePartsService;
        this.activeRoute = activeRoute;
        this.isEnabled = true;
        this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (routeChange) {
            _this.routeParts = _this.routePartsService.generateRouteParts(_this.activeRoute.snapshot);
            // generate url from parts
            _this.routeParts.reverse().map(function (item, i) {
                item.breadcrumb = _this.parseText(item);
                item.urlSegments.forEach(function (urlSegment, j) {
                    if (j === 0)
                        return item.url = "" + urlSegment.path;
                    item.url += "/" + urlSegment.path;
                });
                if (i === 0) {
                    return item;
                }
                // prepend previous part to current part
                item.url = _this.routeParts[i - 1].url + "/" + item.url;
                return item;
            });
        });
    }
    BreadcrumbComponent.prototype.ngOnInit = function () { };
    BreadcrumbComponent.prototype.parseText = function (part) {
        part.breadcrumb = part.breadcrumb.replace(/{{([^{}]*)}}/g, function (a, b) {
            var r = part.params[b];
            return typeof r === 'string' ? r : a;
        });
        return part.breadcrumb;
    };
    return BreadcrumbComponent;
}());
BreadcrumbComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-breadcrumb',
        template: __webpack_require__("../../../../../src/app/components/common/breadcrumb/breadcrumb.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/common/breadcrumb/breadcrumb.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_route_parts_route_parts_service__["a" /* RoutePartsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_route_parts_route_parts_service__["a" /* RoutePartsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object])
], BreadcrumbComponent);

var _a, _b, _c;
//# sourceMappingURL=breadcrumb.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/common/customizer/customizer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#app-customizer {\r\n  position: fixed;\r\n  z-index: 100;\r\n  bottom: 16px;\r\n  right: 24px;\r\n}\r\n#app-customizer .handle {\r\n  float: right;\r\n}\r\n#app-customizer .mat-card-content  {\r\n  padding: 1rem 1.5rem 2rem;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/common/customizer/customizer.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"app-customizer\">\n  <div class=\"handle\" *ngIf=\"!isCustomizerOpen\">\n    <button \n    mat-fab\n    color=\"warn\" \n    (click)=\"isCustomizerOpen = true\">\n      <mat-icon>settings</mat-icon>\n    </button>\n  </div>\n  <mat-card class=\"p-0\" *ngIf=\"isCustomizerOpen\">\n    <mat-card-title class=\"mat-bg-warn\">\n      <div class=\"card-title-text\">\n        <span>Settings</span>\n        <span fxFlex></span>\n        <button \n        class=\"card-control\" \n        mat-icon-button\n        (click)=\"isCustomizerOpen = false\">\n          <mat-icon>close</mat-icon>\n        </button>\n      </div>\n    </mat-card-title>\n    <mat-card-content>\n      <div class=\"pb-1\">\n        <h5 class=\"m-0 pb-1\">Choose a Navigation Style</h5>\n        <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"selectedMenu\" (change)=\"changeSidenav($event)\">\n          <mat-radio-button \n          *ngFor=\"let type of sidenavTypes\" \n          [value]=\"type.value\">\n            {{type.name}}\n          </mat-radio-button>\n        </mat-radio-group>\n      </div>\n      <mat-divider></mat-divider>\n      <div class=\"pt-1 pb-1\">\n        <mat-checkbox [(ngModel)]=\"isBreadcrumbEnabled\" (change)=\"toggleBreadcrumb($event)\">Use breadcrumb</mat-checkbox>\n      </div>\n    </mat-card-content>\n  </mat-card>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/common/customizer/customizer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomizerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_navigation_navigation_service__ = __webpack_require__("../../../../../src/app/services/navigation/navigation.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CustomizerComponent = (function () {
    function CustomizerComponent(navService) {
        this.navService = navService;
        this.isCustomizerOpen = false;
        this.selectedMenu = 'icon-menu';
        this.isBreadcrumbEnabled = true;
        this.sidenavTypes = [{
                name: 'Default Menu',
                value: 'default-menu'
            }, {
                name: 'Separator Menu',
                value: 'separator-menu'
            }, {
                name: 'Icon Menu',
                value: 'icon-menu'
            }];
    }
    CustomizerComponent.prototype.ngOnInit = function () { };
    CustomizerComponent.prototype.changeSidenav = function (data) {
        this.navService.publishNavigationChange(data.value);
    };
    CustomizerComponent.prototype.toggleBreadcrumb = function (data) {
        this.breadcrumb.isEnabled = data.checked;
    };
    return CustomizerComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], CustomizerComponent.prototype, "breadcrumb", void 0);
CustomizerComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-customizer',
        template: __webpack_require__("../../../../../src/app/components/common/customizer/customizer.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/common/customizer/customizer.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_navigation_navigation_service__["a" /* NavigationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_navigation_navigation_service__["a" /* NavigationService */]) === "function" && _a || Object])
], CustomizerComponent);

var _a;
//# sourceMappingURL=customizer.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/common/layouts/admin-layout/admin-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate_ng2_translate__ = __webpack_require__("../../../../ng2-translate/ng2-translate.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_theme_theme_service__ = __webpack_require__("../../../../../src/app/services/theme/theme.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_perfect_scrollbar__ = __webpack_require__("../../../../perfect-scrollbar/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_perfect_scrollbar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_perfect_scrollbar__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AdminLayoutComponent = (function () {
    function AdminLayoutComponent(router, translate, themeService) {
        var _this = this;
        this.router = router;
        this.translate = translate;
        this.themeService = themeService;
        this.isSidenavOpen = false;
        // Close sidenav after route change in mobile
        router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (routeChange) {
            _this.url = routeChange.url;
            if (_this.isNavOver()) {
                _this.sideNave.close();
            }
        });
        // Translator init
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
        // Initialize Perfect scrollbar for sidenav
        var navigationHold = document.getElementById('scroll-area');
        __WEBPACK_IMPORTED_MODULE_5_perfect_scrollbar__["initialize"](navigationHold, {
            suppressScrollX: true
        });
        console.log(this.themeService.activatedThemeName);
    };
    AdminLayoutComponent.prototype.isNavOver = function () {
        return window.matchMedia("(max-width: 960px)").matches;
    };
    return AdminLayoutComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["A" /* MatSidenav */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_material__["A" /* MatSidenav */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_material__["A" /* MatSidenav */]) === "function" && _a || Object)
], AdminLayoutComponent.prototype, "sideNave", void 0);
AdminLayoutComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-admin-layout',
        template: __webpack_require__("../../../../../src/app/components/common/layouts/admin-layout/admin-layout.template.html")
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_translate_ng2_translate__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ng2_translate_ng2_translate__["c" /* TranslateService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_theme_theme_service__["a" /* ThemeService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_theme_theme_service__["a" /* ThemeService */]) === "function" && _d || Object])
], AdminLayoutComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=admin-layout.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/common/layouts/admin-layout/admin-layout.template.html":
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"app-side-nav-container\">\r\n  <!-- Main side navigation -->\r\n  <mat-sidenav #sidenav [opened]=\"!isNavOver()\" [mode]=\"isNavOver() ? 'over' : 'side'\" class=\"sidebar-panel\">\r\n    <div id=\"scroll-area\" class=\"navigation-hold\" fxLayout=\"column\">\r\n      <!-- App Logo -->\r\n      <div class=\"branding default-bg\">\r\n          <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\r\n          <!-- Two different logos for dark and light themes -->\r\n          <img \r\n          src=\"assets/images/logo-text-white.png\" \r\n          alt=\"\" \r\n          class=\"app-logo-text\"\r\n          *ngIf=\"themeService.activatedThemeName.indexOf('dark') !== -1\">\r\n          <img \r\n          src=\"assets/images/logo-text.png\" \r\n          alt=\"\" \r\n          class=\"app-logo-text\"\r\n          *ngIf=\"themeService.activatedThemeName.indexOf('dark') === -1\">\r\n      </div>\r\n\r\n      <!-- Sidebar user -->\r\n      <div class=\"app-user\">\r\n        <div class=\"app-user-photo\">\r\n          <img src=\"assets/images/face-7.jpg\" alt=\"\">\r\n        </div>\r\n        <span class=\"app-user-name mb-05\">\r\n          <mat-icon class=\"icon-xs text-muted\">lock</mat-icon> \r\n          Watson Joyce\r\n        </span>\r\n        <!-- Small buttons -->\r\n        <div class=\"app-user-controls\">\r\n          <button \r\n          class=\"text-muted\"\r\n          mat-icon-button \r\n          mat-xs-button\r\n          [matMenuTriggerFor]=\"appUserMenu\">\r\n            <mat-icon>settings</mat-icon>\r\n          </button>\r\n          <button \r\n          class=\"text-muted\"\r\n          mat-icon-button \r\n          mat-xs-button\r\n          matTooltip=\"Inbox\"\r\n          routerLink=\"/inbox\">\r\n            <mat-icon>email</mat-icon>\r\n          </button>\r\n          <button \r\n          class=\"text-muted\"\r\n          mat-icon-button \r\n          mat-xs-button\r\n          matTooltip=\"Sign Out\"\r\n          routerLink=\"/sessions/signin\">\r\n            <mat-icon>exit_to_app</mat-icon>\r\n          </button>\r\n          <mat-menu #appUserMenu=\"matMenu\">\r\n              <button mat-menu-item routerLink=\"/profile/overview\">\r\n                <mat-icon>account_box</mat-icon>\r\n                <span>Profile</span>\r\n              </button>\r\n              <button mat-menu-item routerLink=\"/profile/settings\">\r\n                <mat-icon>settings</mat-icon>\r\n                <span>Account Settings</span>\r\n              </button>\r\n              <button mat-menu-item>\r\n                <mat-icon>notifications_off</mat-icon>\r\n                <span>Disable alerts</span>\r\n              </button>\r\n              <button mat-menu-item routerLink=\"/sessions/signin\">\r\n                <mat-icon>exit_to_app</mat-icon>\r\n                <span>Sign out</span>\r\n              </button>\r\n            </mat-menu>\r\n        </div>\r\n      </div>\r\n      <!-- Navigation -->\r\n      <navigation></navigation>\r\n    </div>\r\n  </mat-sidenav>\r\n\r\n  <!-- Top Bar -->\r\n  <!-- Template reference variables of Left sidebar and Right notification is supplied -->\r\n  <!-- Listens language change event -->\r\n  <topbar \r\n  [sidenav]=\"sidenav\" \r\n  [notificPanel]=\"notificationPanel\"\r\n  (onLangChange)=\"translate.use($event)\"></topbar>\r\n  <!-- App content -->\r\n  <app-breadcrumb #breadcrumb></app-breadcrumb>\r\n  <div class=\"rightside-content-hold\">\r\n    <router-outlet></router-outlet>\r\n  </div>\r\n  <!-- Notificaation bar -->\r\n  <mat-sidenav #notificationPanel mode=\"over\" class=\"\" align=\"end\">\r\n    <div class=\"navigation-hold\" fxLayout=\"column\">\r\n      <app-notifications [notificPanel]=\"notificationPanel\"></app-notifications>\r\n    </div>\r\n  </mat-sidenav>\r\n</mat-sidenav-container>\r\n\r\n<!-- Only of demo purpose -->\r\n<!-- Remove this from your production version -->\r\n<app-customizer [breadcrumb]=\"breadcrumb\"></app-customizer>"

/***/ }),

/***/ "../../../../../src/app/components/common/layouts/auth-layout/auth-layout.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/common/layouts/auth-layout/auth-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "../../../../../src/app/components/common/layouts/auth-layout/auth-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthLayoutComponent = (function () {
    function AuthLayoutComponent() {
    }
    AuthLayoutComponent.prototype.ngOnInit = function () {
    };
    return AuthLayoutComponent;
}());
AuthLayoutComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-auth-layout',
        template: __webpack_require__("../../../../../src/app/components/common/layouts/auth-layout/auth-layout.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/common/layouts/auth-layout/auth-layout.component.css")]
    }),
    __metadata("design:paramtypes", [])
], AuthLayoutComponent);

//# sourceMappingURL=auth-layout.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/common/navigation/navigation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_navigation_navigation_service__ = __webpack_require__("../../../../../src/app/services/navigation/navigation.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavigationComponent = (function () {
    function NavigationComponent(navService) {
        this.navService = navService;
    }
    NavigationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.iconTypeMenuTitle = this.navService.iconTypeMenuTitle;
        // Loads menu items from NavigationService
        this.navService.menuItems$.subscribe(function (menuItem) {
            _this.menuItems = menuItem;
            //Checks item list has any icon type.
            _this.hasIconTypeMenuItem = !!_this.menuItems.filter(function (item) { return item.type === 'icon'; }).length;
        });
    };
    // Only for demo purpose
    NavigationComponent.prototype.addMenuItem = function () {
        this.menuItems.push({
            name: 'ITEM',
            type: 'dropDown',
            tooltip: 'Item',
            icon: 'done',
            state: 'material',
            sub: [
                { name: 'SUBITEM', state: 'cards' },
                { name: 'SUBITEM', state: 'buttons' }
            ]
        });
    };
    return NavigationComponent;
}());
NavigationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'navigation',
        template: __webpack_require__("../../../../../src/app/components/common/navigation/navigation.template.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_navigation_navigation_service__["a" /* NavigationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_navigation_navigation_service__["a" /* NavigationService */]) === "function" && _a || Object])
], NavigationComponent);

var _a;
//# sourceMappingURL=navigation.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/common/navigation/navigation.template.html":
/***/ (function(module, exports) {

module.exports = "<mat-nav-list class=\"\" role=\"list\">\r\n  <!--==== Side menu items ====-->\r\n  <div class=\"icon-menu\" *ngIf=\"hasIconTypeMenuItem\">\r\n    <!-- Icon menu separator -->\r\n    <div class=\"mt-1 mb-1 icon-menu-separator\">\r\n       <mat-divider [style.margin]=\"'0 -24px'\"></mat-divider> \r\n      <span [ngStyle]=\"{paddingLeft: 0, marginLeft: '-6px'}\" class=\"nav-section-title text-muted\" >{{iconTypeMenuTitle}}</span>\r\n    </div>\r\n    <!-- Icon menu items -->\r\n    <div class=\"icon-menu-item\" *ngFor=\"let item of menuItems\">\r\n      <button\r\n      *ngIf=\"!item.disabled && item.type === 'icon'\"\r\n      mat-raised-button\r\n      [matTooltip]=\"item.tooltip\"\r\n      \r\n      routerLink=\"/{{item.state}}\"\r\n      routerLinkActive=\"mat-bg-primary\"><mat-icon>{{item.icon}}</mat-icon></button>\r\n    </div>\r\n  </div>\r\n  <!-- Regular menu items -->\r\n  <div *ngFor=\"let item of menuItems\">\r\n    <!-- if it's not desabled and not a separator and not icon -->\r\n    <mat-list-item \r\n    sideNavAccordion \r\n    class=\"sidebar-list-item\" \r\n    role=\"listitem\"\r\n    *ngIf=\"!item.disabled && item.type !== 'separator' && item.type !== 'icon'\"\r\n    [ngClass]=\"{'has-submenu': item.type === 'dropDown'}\"\r\n    routerLinkActive=\"open\">\r\n      <a routerLink=\"/{{item.state}}\" *ngIf=\"item.type === 'link'\">\r\n        <span\r\n        class=\"menu-item-tooltip\" \r\n        [matTooltip]=\"item.tooltip\" \r\n        matTooltipPosition=\"right\"></span>\r\n        <mat-icon>{{item.icon}}</mat-icon>\r\n        <span>{{item.name | translate}}</span>\r\n      </a>\r\n      <a *ngIf=\"item.type === 'dropDown'\">\r\n        <span\r\n        class=\"menu-item-tooltip\" \r\n        [matTooltip]=\"item.tooltip\" \r\n        matTooltipPosition=\"above\"></span>\r\n        <mat-icon>{{item.icon}}</mat-icon>\r\n        <span>{{item.name | translate}}</span>\r\n        <span fxFlex></span>\r\n        <mat-icon class=\"menu-caret\">arrow_drop_down</mat-icon>\r\n      </a>\r\n      <a [href]=\"item.state\" *ngIf=\"item.type === 'extLink'\" target=\"_blank\">\r\n        <span\r\n        class=\"menu-item-tooltip\" \r\n        [matTooltip]=\"item.tooltip\" \r\n        matTooltipPosition=\"right\"></span>\r\n        <mat-icon>{{item.icon}}</mat-icon>\r\n        <span>{{item.name | translate}}</span>\r\n      </a>\r\n      <mat-nav-list class=\"sub-menu\" role=\"list\" *ngIf=\"item.type === 'dropDown'\">\r\n        <mat-list-item \r\n        routerLinkActive=\"selected\"\r\n        *ngFor=\"let subItem of item.sub\">\r\n          <a routerLink=\"{{item.state ? '/'+item.state : ''}}/{{subItem.state}}\">{{subItem.name | translate}}</a>\r\n        </mat-list-item>\r\n      </mat-nav-list>\r\n    </mat-list-item>\r\n\r\n    <!-- If item type is separator -->\r\n    <div class=\"mt-1 mb-1\" *ngIf=\"!item.disabled && item.type === 'separator'\">\r\n      <mat-divider></mat-divider>\r\n      <span class=\"nav-section-title text-muted\" *ngIf=\"!!item.name\">{{item.name | translate}}</span>\r\n    </div>\r\n  </div>\r\n\r\n  <!-- Only for demo purpose -->\r\n  <mat-divider></mat-divider>\r\n  <div>\r\n    <mat-list-item class=\"sidebar-list-item\">\r\n      <a (click)=\"addMenuItem()\">\r\n        <span\r\n        class=\"menu-item-tooltip\" \r\n        matTooltip=\"Add new menu item\" \r\n        matTooltipPosition=\"right\"></span>\r\n        <mat-icon>add</mat-icon>\r\n        <span>{{'ADD' | translate}}</span>\r\n      </a>\r\n    </mat-list-item>\r\n  </div>\r\n</mat-nav-list>"

/***/ }),

/***/ "../../../../../src/app/components/common/notifications/notifications.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/common/notifications/notifications.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center mat-bg-primary pt-1 pb-1\">\r\n  <h6 class=\"m-0\">Notifications</h6>\r\n</div>\r\n<mat-nav-list class=\"notification-list\" role=\"list\">\r\n  <!-- Notification item -->\r\n  <mat-list-item *ngFor=\"let n of notifications\" class=\"notific-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n    <mat-icon [color]=\"n.color\" class=\"notific-icon\">{{n.icon}}</mat-icon>\r\n    <a [routerLink]=\"[n.route || '/dashboard']\">\r\n      <div class=\"mat-list-text\">\r\n        <h4 class=\"message\">{{n.message}}</h4>\r\n        <small class=\"time text-muted\">{{n.time}}</small>\r\n      </div>\r\n    </a>\r\n  </mat-list-item>\r\n</mat-nav-list>\r\n<div class=\"text-center mt-1\" *ngIf=\"notifications.length\">\r\n  <small><a href=\"#\" (click)=\"clearAll($event)\">Clear all notifications</a></small>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/common/notifications/notifications.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationsComponent = (function () {
    function NotificationsComponent(router) {
        this.router = router;
        // Dummy notifications
        this.notifications = [{
                message: 'New contact added',
                icon: 'assignment_ind',
                time: '1 min ago',
                route: '/inbox',
                color: 'primary'
            }, {
                message: 'New message',
                icon: 'chat',
                time: '4 min ago',
                route: '/chat',
                color: 'accent'
            }, {
                message: 'Server rebooted',
                icon: 'settings_backup_restore',
                time: '12 min ago',
                route: '/charts',
                color: 'warn'
            }];
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.subscribe(function (routeChange) {
            if (routeChange instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */]) {
                _this.notificPanel.close();
            }
        });
    };
    NotificationsComponent.prototype.clearAll = function (e) {
        e.preventDefault();
        this.notifications = [];
    };
    return NotificationsComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], NotificationsComponent.prototype, "notificPanel", void 0);
NotificationsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-notifications',
        template: __webpack_require__("../../../../../src/app/components/common/notifications/notifications.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/common/notifications/notifications.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object])
], NotificationsComponent);

var _a;
//# sourceMappingURL=notifications.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/common/topbar/topbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TopbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__ = __webpack_require__("../../../../../src/app/helpers/dom.helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_theme_theme_service__ = __webpack_require__("../../../../../src/app/services/theme/theme.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TopbarComponent = (function () {
    function TopbarComponent(themeService) {
        this.themeService = themeService;
        this.onLangChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.currentLang = 'en';
        this.availableLangs = [{
                name: 'English',
                code: 'en',
            }, {
                name: 'Spanish',
                code: 'es',
            }];
    }
    TopbarComponent.prototype.ngOnInit = function () {
        this.egretThemes = this.themeService.egretThemes;
    };
    TopbarComponent.prototype.setLang = function () {
        this.onLangChange.emit(this.currentLang);
    };
    TopbarComponent.prototype.changeTheme = function (theme) {
        this.themeService.changeTheme(theme);
    };
    TopbarComponent.prototype.toggleNotific = function () {
        this.notificPanel.toggle();
    };
    TopbarComponent.prototype.toggleSidenav = function () {
        this.sidenav.toggle();
    };
    TopbarComponent.prototype.toggleCollapse = function () {
        var appBody = document.body;
        __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["f" /* toggleClass */](appBody, 'collapsed-menu');
        __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["e" /* removeClass */](document.getElementsByClassName('has-submenu'), 'open');
    };
    return TopbarComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], TopbarComponent.prototype, "sidenav", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], TopbarComponent.prototype, "notificPanel", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], TopbarComponent.prototype, "onLangChange", void 0);
TopbarComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'topbar',
        template: __webpack_require__("../../../../../src/app/components/common/topbar/topbar.template.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_theme_theme_service__["a" /* ThemeService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_theme_theme_service__["a" /* ThemeService */]) === "function" && _a || Object])
], TopbarComponent);

var _a;
//# sourceMappingURL=topbar.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/common/topbar/topbar.template.html":
/***/ (function(module, exports) {

module.exports = "<mat-toolbar class=\"topbar\">\r\n  <!-- Sidenav toggle button -->\r\n  <button \r\n  mat-icon-button\r\n  id=\"sidenavToggle\" \r\n  (click)=\"toggleSidenav()\"\r\n  mat-tooltip=\"Toggle Hide/Open\">\r\n  <mat-icon>menu</mat-icon>\r\n  </button>\r\n  <!-- Sidenav toggle collapse -->\r\n  <button \r\n  *ngIf=\"sidenav.opened\"\r\n  mat-icon-button\r\n  id=\"collapseToggle\"\r\n  fxHide.lt-mat=\"true\" \r\n  (click)=\"toggleCollapse()\"\r\n  mat-tooltip=\"Toggle Collapse\"\r\n  class=\"toggle-collapsed\">\r\n  <mat-icon>chevron_left</mat-icon>\r\n  </button>\r\n  <!-- Search form -->\r\n  <div \r\n  fxFlex\r\n  fxHide.lt-sm=\"true\" \r\n  class=\"search-bar\">\r\n    <form class=\"top-search-form\">\r\n      <mat-icon role=\"img\">search</mat-icon>\r\n      <input autofocus=\"true\" placeholder=\"Search\" type=\"text\">\r\n    </form>\r\n  </div>\r\n  <span fxFlex></span>\r\n  <!-- Language Switcher -->\r\n  <mat-select \r\n  placeholder=\"\"\r\n  id=\"langToggle\"\r\n  [style.marginTop]=\"'-16px'\"\r\n  name=\"currentLang\"\r\n  [(ngModel)]=\"currentLang\" \r\n  (change)=\"setLang()\">\r\n    <mat-option \r\n    *ngFor=\"let lang of availableLangs\" \r\n    [value]=\"lang.code\" ngDefaultControl>{{ lang.name }}</mat-option>\r\n  </mat-select>\r\n  <!-- Theme Switcher -->\r\n  <button \r\n  mat-icon-button\r\n  id=\"schemeToggle\" \r\n  [style.overflow]=\"'visible'\"\r\n  matTooltip=\"Color Schemes\"\r\n  [matMenuTriggerFor]=\"themeMenu\"\r\n  class=\"topbar-button-right\">\r\n    <mat-icon>format_color_fill</mat-icon>\r\n  </button>\r\n  <mat-menu #themeMenu=\"matMenu\">\r\n    <mat-grid-list\r\n    class=\"theme-list\" \r\n    cols=\"2\" \r\n    rowHeight=\"48px\">\r\n      <mat-grid-tile \r\n      *ngFor=\"let theme of egretThemes\"\r\n      (click)=\"changeTheme(theme)\">\r\n        <div mat-menu-item [title]=\"theme.name\">\r\n          <div [style.background]=\"theme.baseColor\" class=\"egret-swatch\"></div>\r\n          <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n        </div>\r\n      </mat-grid-tile>\r\n    </mat-grid-list>\r\n  </mat-menu>\r\n  <!-- Notification toggle button -->\r\n  <button \r\n  mat-icon-button\r\n  matTooltip=\"Notifications\" \r\n  (click)=\"toggleNotific()\"\r\n  [style.overflow]=\"'visible'\" \r\n  class=\"topbar-button-right\">\r\n    <mat-icon>notifications</mat-icon>\r\n    <span class=\"notification-number mat-bg-warn\">3</span>\r\n  </button>\r\n  <!-- Top left user menu -->\r\n  <button mat-icon-button [matMenuTriggerFor]=\"accountMenu\" class=\"topbar-button-right img-button\">\r\n    <img src=\"assets/images/face-7.jpg\" alt=\"\">\r\n  </button>\r\n  <mat-menu #accountMenu=\"matMenu\">\r\n    <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n      <mat-icon>account_box</mat-icon>\r\n      <span>Profile</span>\r\n    </button>\r\n    <button mat-menu-item [routerLink]=\"['/profile/settings']\">\r\n      <mat-icon>settings</mat-icon>\r\n      <span>Account Settings</span>\r\n    </button>\r\n    <button mat-menu-item>\r\n      <mat-icon>notifications_off</mat-icon>\r\n      <span>Disable alerts</span>\r\n    </button>\r\n    <button mat-menu-item [routerLink]=\"['/sessions/signin']\">\r\n      <mat-icon>exit_to_app</mat-icon>\r\n      <span>Sign out</span>\r\n    </button>\r\n  </mat-menu>\r\n</mat-toolbar>"

/***/ }),

/***/ "../../../../../src/app/directives/common/app-accordion.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppAccordionDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__ = __webpack_require__("../../../../../src/app/helpers/dom.helper.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppAccordionDirective = (function () {
    function AppAccordionDirective(el) {
        this.el = el;
    }
    AppAccordionDirective.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.el.nativeElement.className += 'accordion-handle';
            if (__WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["d" /* hasClass */](_this.el.nativeElement, 'app-accordion')) {
                _this.parentLi = _this.el.nativeElement;
            }
            else {
                _this.parentLi = __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["c" /* findClosest */](_this.el.nativeElement, 'app-accordion');
            }
        });
    };
    AppAccordionDirective.prototype.onClick = function ($event) {
        this.toggleOpen();
    };
    AppAccordionDirective.prototype.toggleOpen = function () {
        var accordionItems = document.getElementsByClassName('app-accordion');
        if (__WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["d" /* hasClass */](this.parentLi, 'open')) {
            __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["e" /* removeClass */](accordionItems, 'open');
        }
        else {
            __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["e" /* removeClass */](accordionItems, 'open');
            __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["a" /* addClass */](this.parentLi, 'open');
        }
    };
    return AppAccordionDirective;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AppAccordionDirective.prototype, "onClick", null);
AppAccordionDirective = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({ selector: '[appAccordion]' }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object])
], AppAccordionDirective);

var _a;
//# sourceMappingURL=app-accordion.directive.js.map

/***/ }),

/***/ "../../../../../src/app/directives/common/common-directives.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonDirectivesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__equal_validator_directive__ = __webpack_require__("../../../../../src/app/directives/common/equal-validator.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sidenav_accordion_directive__ = __webpack_require__("../../../../../src/app/directives/common/sidenav-accordion.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_accordion_directive__ = __webpack_require__("../../../../../src/app/directives/common/app-accordion.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__font_size_directive__ = __webpack_require__("../../../../../src/app/directives/common/font-size.directive.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CommonDirectivesModule = (function () {
    function CommonDirectivesModule() {
    }
    return CommonDirectivesModule;
}());
CommonDirectivesModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__equal_validator_directive__["a" /* EqualValidatorDirective */],
            __WEBPACK_IMPORTED_MODULE_3__sidenav_accordion_directive__["a" /* SideNavAccordionDirective */],
            __WEBPACK_IMPORTED_MODULE_4__app_accordion_directive__["a" /* AppAccordionDirective */],
            __WEBPACK_IMPORTED_MODULE_5__font_size_directive__["a" /* FontSizeDirective */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__equal_validator_directive__["a" /* EqualValidatorDirective */],
            __WEBPACK_IMPORTED_MODULE_3__sidenav_accordion_directive__["a" /* SideNavAccordionDirective */],
            __WEBPACK_IMPORTED_MODULE_4__app_accordion_directive__["a" /* AppAccordionDirective */],
            __WEBPACK_IMPORTED_MODULE_5__font_size_directive__["a" /* FontSizeDirective */]
        ]
    })
], CommonDirectivesModule);

//# sourceMappingURL=common-directives.module.js.map

/***/ }),

/***/ "../../../../../src/app/directives/common/equal-validator.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EqualValidatorDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var EqualValidatorDirective = EqualValidatorDirective_1 = (function () {
    function EqualValidatorDirective(validateEqual, reverse) {
        this.validateEqual = validateEqual;
        this.reverse = reverse;
    }
    Object.defineProperty(EqualValidatorDirective.prototype, "isReverse", {
        get: function () {
            if (!this.reverse)
                return false;
            return this.reverse === 'true' ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    EqualValidatorDirective.prototype.validate = function (currentControl) {
        // self value
        var currentControlValue = currentControl.value;
        // control vlaue
        var anotherControl = currentControl.root.get(this.validateEqual);
        var anotherControlValue = anotherControl ? anotherControl.value : null;
        // value not equal
        if (anotherControl && currentControlValue !== anotherControlValue && !this.isReverse) {
            return {
                validateEqual: true
            };
        }
        // value equal and reverse
        if (anotherControl && currentControlValue === anotherControlValue && this.isReverse) {
            delete anotherControl.errors['validateEqual'];
            if (!Object.keys(anotherControl.errors).length)
                anotherControl.setErrors(null);
        }
        // value not equal and reverse
        if (anotherControl && currentControlValue !== anotherControlValue && this.isReverse) {
            anotherControl.setErrors({ validateEqual: true });
        }
        return null;
    };
    return EqualValidatorDirective;
}());
EqualValidatorDirective = EqualValidatorDirective_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[appEqualValidator][ngModel]',
        providers: [
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["NG_VALIDATORS"], useExisting: EqualValidatorDirective_1, multi: true }
        ]
    }),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Attribute"])('appEqualValidator')),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Attribute"])('reverse')),
    __metadata("design:paramtypes", [String, String])
], EqualValidatorDirective);

var EqualValidatorDirective_1;
//# sourceMappingURL=equal-validator.directive.js.map

/***/ }),

/***/ "../../../../../src/app/directives/common/font-size.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FontSizeDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};

var FontSizeDirective = (function () {
    function FontSizeDirective(fontSize, el) {
        this.fontSize = fontSize;
        this.el = el;
    }
    FontSizeDirective.prototype.ngOnInit = function () {
        this.el.nativeElement.fontSize = this.fontSize;
    };
    return FontSizeDirective;
}());
FontSizeDirective = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({ selector: '[fontSize]' }),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Attribute"])('fontSize')),
    __metadata("design:paramtypes", [String, typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object])
], FontSizeDirective);

var _a;
//# sourceMappingURL=font-size.directive.js.map

/***/ }),

/***/ "../../../../../src/app/directives/common/sidenav-accordion.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideNavAccordionDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__ = __webpack_require__("../../../../../src/app/helpers/dom.helper.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SideNavAccordionDirective = (function () {
    function SideNavAccordionDirective(el) {
        this.el = el;
    }
    SideNavAccordionDirective.prototype.ngOnInit = function () {
        var self = this;
        var subMenu = this.el.nativeElement.querySelector('.mat-list-item-content > md-nav-list');
        var isCollapsed = __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["d" /* hasClass */](document.body, 'collapsed-menu');
        if (!!subMenu)
            this.el.nativeElement.className += ' has-submenu';
        // remove open class that is added my router
        if (isCollapsed) {
            setTimeout(function () {
                __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["e" /* removeClass */](self.el.nativeElement, 'open');
            });
        }
    };
    SideNavAccordionDirective.prototype.onClick = function ($event) {
        var parentLi = __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["c" /* findClosest */]($event.target, 'mat-list-item');
        if (!__WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["d" /* hasClass */](parentLi, 'has-submenu')) {
            // PREVENTS CLOSING PARENT ITEM
            return;
        }
        ;
        this.toggleOpen();
    };
    // For collapsed sidebar
    SideNavAccordionDirective.prototype.onMouseEnter = function ($event) {
        var elem = this.el.nativeElement;
        var isCollapsed = __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["d" /* hasClass */](document.body, 'collapsed-menu');
        if (!isCollapsed)
            return;
        __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["a" /* addClass */](elem, 'open');
    };
    SideNavAccordionDirective.prototype.onMouseLeave = function ($event) {
        var elem = this.el.nativeElement;
        var isCollapsed = __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["d" /* hasClass */](document.body, 'collapsed-menu');
        if (!isCollapsed)
            return;
        __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["e" /* removeClass */](elem, 'open');
    };
    SideNavAccordionDirective.prototype.toggleOpen = function () {
        var elem = this.el.nativeElement;
        var parenMenuItems = document.getElementsByClassName('has-submenu');
        if (__WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["d" /* hasClass */](elem, 'open')) {
            __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["e" /* removeClass */](parenMenuItems, 'open');
        }
        else {
            __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["e" /* removeClass */](parenMenuItems, 'open');
            __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["a" /* addClass */](elem, 'open');
        }
    };
    return SideNavAccordionDirective;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SideNavAccordionDirective.prototype, "onClick", null);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('mouseenter', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SideNavAccordionDirective.prototype, "onMouseEnter", null);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('mouseleave', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SideNavAccordionDirective.prototype, "onMouseLeave", null);
SideNavAccordionDirective = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({ selector: '[sideNavAccordion]' }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object])
], SideNavAccordionDirective);

var _a;
//# sourceMappingURL=sidenav-accordion.directive.js.map

/***/ }),

/***/ "../../../../../src/app/helpers/dom.helper.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["e"] = removeClass;
/* harmony export (immutable) */ __webpack_exports__["a"] = addClass;
/* harmony export (immutable) */ __webpack_exports__["c"] = findClosest;
/* harmony export (immutable) */ __webpack_exports__["d"] = hasClass;
/* harmony export (immutable) */ __webpack_exports__["f"] = toggleClass;
/* harmony export (immutable) */ __webpack_exports__["b"] = changeTheme;
/* unused harmony export ieChatjsFix */
function removeClass(el, className) {
    if (!el || el.length === 0)
        return;
    if (!el.length) {
        el.classList.remove(className);
    }
    else {
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove(className);
        }
    }
}
function addClass(el, className) {
    if (!el)
        return;
    if (!el.length) {
        el.classList.add(className);
    }
    else {
        for (var i = 0; i < el.length; i++) {
            el[i].classList.add(className);
        }
    }
}
function findClosest(el, className) {
    if (!el)
        return;
    while (el) {
        var parent = el.parentElement;
        if (parent && hasClass(parent, className)) {
            return parent;
        }
        el = parent;
    }
}
function hasClass(el, className) {
    if (!el)
        return;
    return (" " + el.className + " ").replace(/[\n\t]/g, ' ').indexOf(" " + className + " ") > -1;
}
function toggleClass(el, className) {
    if (!el)
        return;
    if (hasClass(el, className)) {
        removeClass(el, className);
    }
    else {
        addClass(el, className);
    }
}
function changeTheme(themes, themeName) {
    themes.forEach(function (theme) {
        removeClass(document.body, theme.name);
    });
    addClass(document.body, themeName);
}
;
function ieChatjsFix() {
    if (window.hasOwnProperty('MSInputMethodContext') || document.hasOwnProperty('documentMode')) {
        document.body.style.width = '99.9%';
        setTimeout(function () {
            document.body.style.width = '100%';
        });
    }
}
//# sourceMappingURL=dom.helper.js.map

/***/ }),

/***/ "../../../../../src/app/services/auth/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthService = (function () {
    function AuthService(router) {
        this.router = router;
        this.isAuthenticated = true; // Set this value dynamically
    }
    AuthService.prototype.canActivate = function (route, state) {
        if (this.isAuthenticated) {
            return true;
        }
        this.router.navigate(['/sessions/signin']);
        return false;
    };
    return AuthService;
}());
AuthService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object])
], AuthService);

var _a;
//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/navigation/navigation.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavigationService = (function () {
    function NavigationService() {
        this.defaultMenu = [
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'dashboard'
            },
            {
                name: 'INBOX',
                type: 'link',
                tooltip: 'Inbox',
                icon: 'inbox',
                state: 'inbox'
            },
            {
                name: 'CHAT',
                type: 'link',
                tooltip: 'Chat',
                icon: 'chat',
                state: 'chat'
            },
            {
                name: 'CALENDAR',
                type: 'link',
                tooltip: 'Calendar',
                icon: 'date_range',
                state: 'calendar'
            },
            {
                name: 'DIALOGS',
                type: 'dropDown',
                tooltip: 'Dialogs',
                icon: 'filter_none',
                state: 'dialogs',
                sub: [
                    { name: 'CONFIRM', state: 'confirm' },
                    { name: 'LOADER', state: 'loader' },
                ]
            },
            {
                name: 'MATERIAL',
                type: 'dropDown',
                tooltip: 'Material',
                icon: 'favorite',
                state: 'material',
                sub: [
                    { name: 'BUTTONS', state: 'buttons' },
                    { name: 'CARDS', state: 'cards' },
                    { name: 'GRIDS', state: 'grids' },
                    { name: 'LISTS', state: 'lists' },
                    { name: 'MENU', state: 'menu' },
                    { name: 'TABS', state: 'tabs' },
                    { name: 'SELECT', state: 'select' },
                    { name: 'RADIO', state: 'radio' },
                    { name: 'AUTOCOMPLETE', state: 'autocomplete' },
                    { name: 'SLIDER', state: 'slider' },
                    { name: 'PROGRESS', state: 'progress' },
                    { name: 'SNACKBAR', state: 'snackbar' },
                ]
            },
            {
                name: 'FORMS',
                type: 'dropDown',
                tooltip: 'Forms',
                icon: 'description',
                state: 'forms',
                sub: [
                    { name: 'BASIC', state: 'basic' },
                    { name: 'EDITOR', state: 'editor' },
                    { name: 'UPLOAD', state: 'upload' },
                ]
            },
            {
                name: 'TABLES',
                type: 'dropDown',
                tooltip: 'Tables',
                icon: 'format_line_spacing',
                state: 'tables',
                sub: [
                    { name: 'FULLSCREEN', state: 'fullscreen' },
                    { name: 'PAGING', state: 'paging' },
                    { name: 'FILTER', state: 'filter' },
                ]
            },
            {
                name: 'PROFILE',
                type: 'dropDown',
                tooltip: 'Profile',
                icon: 'person',
                state: 'profile',
                sub: [
                    { name: 'OVERVIEW', state: 'overview' },
                    { name: 'SETTINGS', state: 'settings' },
                    { name: 'BLANK', state: 'blank' },
                ]
            },
            {
                name: 'TOUR',
                type: 'link',
                tooltip: 'Tour',
                icon: 'flight_takeoff',
                state: 'tour'
            },
            {
                name: 'MAP',
                type: 'link',
                tooltip: 'Map',
                icon: 'add_location',
                state: 'map'
            },
            {
                name: 'CHARTS',
                type: 'link',
                tooltip: 'Charts',
                icon: 'show_chart',
                state: 'charts'
            },
            {
                name: 'DND',
                type: 'link',
                tooltip: 'Drag and Drop',
                icon: 'adjust',
                state: 'dragndrop'
            },
            {
                name: 'SESSIONS',
                type: 'dropDown',
                tooltip: 'Pages',
                icon: 'view_carousel',
                state: 'sessions',
                sub: [
                    { name: 'SIGNUP', state: 'signup' },
                    { name: 'SIGNIN', state: 'signin' },
                    { name: 'FORGOT', state: 'forgot-password' },
                    { name: 'LOCKSCREEN', state: 'lockscreen' },
                    { name: 'NOTFOUND', state: '404' },
                    { name: 'ERROR', state: 'error' }
                ]
            },
            {
                name: 'OTHERS',
                type: 'dropDown',
                tooltip: 'Others',
                icon: 'blur_on',
                state: 'others',
                sub: [
                    { name: 'GALLERY', state: 'gallery' },
                    { name: 'PRICINGS', state: 'pricing' },
                    { name: 'USERS', state: 'users' },
                    { name: 'BLANK', state: 'blank' },
                ]
            },
            {
                name: 'MATICONS',
                type: 'link',
                tooltip: 'Material Icons',
                icon: 'store',
                state: 'icons'
            },
            {
                name: 'DOC',
                type: 'extLink',
                tooltip: 'Documentation',
                icon: 'library_books',
                state: 'http://egret-doc.mhrafi.com/'
            }
        ];
        this.separatorMenu = [
            {
                type: 'separator',
                name: 'Custom components'
            },
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'dashboard'
            },
            {
                name: 'INBOX',
                type: 'link',
                tooltip: 'Inbox',
                icon: 'inbox',
                state: 'inbox'
            },
            {
                name: 'CHAT',
                type: 'link',
                tooltip: 'Chat',
                icon: 'chat',
                state: 'chat'
            },
            {
                name: 'DIALOGS',
                type: 'dropDown',
                tooltip: 'Dialogs',
                icon: 'filter_none',
                state: 'dialogs',
                sub: [
                    { name: 'CONFIRM', state: 'confirm' },
                    { name: 'LOADER', state: 'loader' },
                ]
            },
            {
                name: 'PROFILE',
                type: 'dropDown',
                tooltip: 'Profile',
                icon: 'person',
                state: 'profile',
                sub: [
                    { name: 'OVERVIEW', state: 'overview' },
                    { name: 'SETTINGS', state: 'settings' },
                    { name: 'BLANK', state: 'blank' },
                ]
            },
            {
                name: 'TOUR',
                type: 'link',
                tooltip: 'Tour',
                icon: 'flight_takeoff',
                state: 'tour'
            },
            {
                type: 'separator',
                name: 'Integrated components'
            },
            {
                name: 'CALENDAR',
                type: 'link',
                tooltip: 'Calendar',
                icon: 'date_range',
                state: 'calendar'
            },
            {
                name: 'MATERIAL',
                type: 'dropDown',
                tooltip: 'Material',
                icon: 'favorite',
                state: 'material',
                sub: [
                    { name: 'BUTTONS', state: 'buttons' },
                    { name: 'CARDS', state: 'cards' },
                    { name: 'GRIDS', state: 'grids' },
                    { name: 'LISTS', state: 'lists' },
                    { name: 'MENU', state: 'menu' },
                    { name: 'TABS', state: 'tabs' },
                    { name: 'SELECT', state: 'select' },
                    { name: 'RADIO', state: 'radio' },
                    { name: 'AUTOCOMPLETE', state: 'autocomplete' },
                    { name: 'SLIDER', state: 'slider' },
                    { name: 'PROGRESS', state: 'progress' },
                    { name: 'SNACKBAR', state: 'snackbar' },
                ]
            },
            {
                name: 'FORMS',
                type: 'dropDown',
                tooltip: 'Forms',
                icon: 'description',
                state: 'forms',
                sub: [
                    { name: 'BASIC', state: 'basic' },
                    { name: 'EDITOR', state: 'editor' },
                    { name: 'UPLOAD', state: 'upload' },
                ]
            },
            {
                name: 'TABLES',
                type: 'dropDown',
                tooltip: 'Tables',
                icon: 'format_line_spacing',
                state: 'tables',
                sub: [
                    { name: 'FULLSCREEN', state: 'fullscreen' },
                    { name: 'PAGING', state: 'paging' },
                    { name: 'FILTER', state: 'filter' },
                ]
            },
            {
                name: 'MAP',
                type: 'link',
                tooltip: 'Map',
                icon: 'add_location',
                state: 'map'
            },
            {
                name: 'CHARTS',
                type: 'link',
                tooltip: 'Charts',
                icon: 'show_chart',
                state: 'charts'
            },
            {
                name: 'DND',
                type: 'link',
                tooltip: 'Drag and Drop',
                icon: 'adjust',
                state: 'dragndrop'
            },
            {
                type: 'separator',
                name: 'Other components'
            },
            {
                name: 'SESSIONS',
                type: 'dropDown',
                tooltip: 'Pages',
                icon: 'view_carousel',
                state: 'sessions',
                sub: [
                    { name: 'SIGNUP', state: 'signup' },
                    { name: 'SIGNIN', state: 'signin' },
                    { name: 'FORGOT', state: 'forgot-password' },
                    { name: 'LOCKSCREEN', state: 'lockscreen' },
                    { name: 'NOTFOUND', state: '404' },
                    { name: 'ERROR', state: 'error' }
                ]
            },
            {
                name: 'OTHERS',
                type: 'dropDown',
                tooltip: 'Others',
                icon: 'blur_on',
                state: 'others',
                sub: [
                    { name: 'GALLERY', state: 'gallery' },
                    { name: 'PRICINGS', state: 'pricing' },
                    { name: 'USERS', state: 'users' },
                    { name: 'BLANK', state: 'blank' },
                ]
            },
            {
                name: 'MATICONS',
                type: 'link',
                tooltip: 'Material Icons',
                icon: 'store',
                state: 'icons'
            },
            {
                name: 'DOC',
                type: 'extLink',
                tooltip: 'Documentation',
                icon: 'library_books',
                state: 'http://egret-doc.mhrafi.com/'
            }
        ];
        this.iconMenu = [
            {
                name: 'CHAT',
                type: 'icon',
                tooltip: 'Chat',
                icon: 'chat',
                state: 'chat'
            },
            {
                name: 'PROFILE',
                type: 'icon',
                tooltip: 'Profile',
                icon: 'person',
                state: 'profile/overview'
            },
            {
                name: 'TOUR',
                type: 'icon',
                tooltip: 'Tour',
                icon: 'flight_takeoff',
                state: 'tour'
            },
            {
                type: 'separator',
                name: 'Main Items'
            },
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'dashboard'
            },
            {
                name: 'INBOX',
                type: 'link',
                tooltip: 'Inbox',
                icon: 'inbox',
                state: 'inbox'
            },
            {
                name: 'CHAT',
                type: 'link',
                tooltip: 'Chat',
                icon: 'chat',
                state: 'chat'
            },
            {
                name: 'CALENDAR',
                type: 'link',
                tooltip: 'Calendar',
                icon: 'date_range',
                state: 'calendar'
            },
            {
                name: 'DIALOGS',
                type: 'dropDown',
                tooltip: 'Dialogs',
                icon: 'filter_none',
                state: 'dialogs',
                sub: [
                    { name: 'CONFIRM', state: 'confirm' },
                    { name: 'LOADER', state: 'loader' },
                ]
            },
            {
                name: 'MATERIAL',
                type: 'dropDown',
                tooltip: 'Material',
                icon: 'favorite',
                state: 'material',
                sub: [
                    { name: 'BUTTONS', state: 'buttons' },
                    { name: 'CARDS', state: 'cards' },
                    { name: 'GRIDS', state: 'grids' },
                    { name: 'LISTS', state: 'lists' },
                    { name: 'MENU', state: 'menu' },
                    { name: 'TABS', state: 'tabs' },
                    { name: 'SELECT', state: 'select' },
                    { name: 'RADIO', state: 'radio' },
                    { name: 'AUTOCOMPLETE', state: 'autocomplete' },
                    { name: 'SLIDER', state: 'slider' },
                    { name: 'PROGRESS', state: 'progress' },
                    { name: 'SNACKBAR', state: 'snackbar' },
                ]
            },
            {
                name: 'FORMS',
                type: 'dropDown',
                tooltip: 'Forms',
                icon: 'description',
                state: 'forms',
                sub: [
                    { name: 'BASIC', state: 'basic' },
                    { name: 'EDITOR', state: 'editor' },
                    { name: 'UPLOAD', state: 'upload' },
                ]
            },
            {
                name: 'TABLES',
                type: 'dropDown',
                tooltip: 'Tables',
                icon: 'format_line_spacing',
                state: 'tables',
                sub: [
                    { name: 'FULLSCREEN', state: 'fullscreen' },
                    { name: 'PAGING', state: 'paging' },
                    { name: 'FILTER', state: 'filter' },
                ]
            },
            {
                name: 'PROFILE',
                type: 'dropDown',
                tooltip: 'Profile',
                icon: 'person',
                state: 'profile',
                sub: [
                    { name: 'OVERVIEW', state: 'overview' },
                    { name: 'SETTINGS', state: 'settings' },
                    { name: 'BLANK', state: 'blank' },
                ]
            },
            {
                name: 'TOUR',
                type: 'link',
                tooltip: 'Tour',
                icon: 'flight_takeoff',
                state: 'tour'
            },
            {
                name: 'MAP',
                type: 'link',
                tooltip: 'Map',
                icon: 'add_location',
                state: 'map'
            },
            {
                name: 'CHARTS',
                type: 'link',
                tooltip: 'Charts',
                icon: 'show_chart',
                state: 'charts'
            },
            {
                name: 'DND',
                type: 'link',
                tooltip: 'Drag and Drop',
                icon: 'adjust',
                state: 'dragndrop'
            },
            {
                name: 'SESSIONS',
                type: 'dropDown',
                tooltip: 'Pages',
                icon: 'view_carousel',
                state: 'sessions',
                sub: [
                    { name: 'SIGNUP', state: 'signup' },
                    { name: 'SIGNIN', state: 'signin' },
                    { name: 'FORGOT', state: 'forgot-password' },
                    { name: 'LOCKSCREEN', state: 'lockscreen' },
                    { name: 'NOTFOUND', state: '404' },
                    { name: 'ERROR', state: 'error' }
                ]
            },
            {
                name: 'OTHERS',
                type: 'dropDown',
                tooltip: 'Others',
                icon: 'blur_on',
                state: 'others',
                sub: [
                    { name: 'GALLERY', state: 'gallery' },
                    { name: 'PRICINGS', state: 'pricing' },
                    { name: 'USERS', state: 'users' },
                    { name: 'BLANK', state: 'blank' },
                ]
            },
            {
                name: 'MATICONS',
                type: 'link',
                tooltip: 'Material Icons',
                icon: 'store',
                state: 'icons'
            },
            {
                name: 'DOC',
                type: 'extLink',
                tooltip: 'Documentation',
                icon: 'library_books',
                state: 'http://egret-doc.mhrafi.com/'
            }
        ];
        // Icon menu TITLE at the very top of navigation.
        // This title will appear if any icon type item is present in menu.
        this.iconTypeMenuTitle = 'Frequently Accessed';
        // sets iconMenu as default;
        this.menuItems = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"](this.iconMenu);
        // navigation component has subscribed to this Observable
        this.menuItems$ = this.menuItems.asObservable();
    }
    // Customizer component uses this method to change menu.
    // You can remove this method and customizer component.
    // Or you can customize this method to supply different menu for
    // different user type.
    NavigationService.prototype.publishNavigationChange = function (menuType) {
        switch (menuType) {
            case 'separator-menu':
                this.menuItems.next(this.separatorMenu);
                break;
            case 'icon-menu':
                this.menuItems.next(this.iconMenu);
                break;
            default:
                this.menuItems.next(this.defaultMenu);
        }
    };
    return NavigationService;
}());
NavigationService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], NavigationService);

//# sourceMappingURL=navigation.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/route-parts/route-parts.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoutePartsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RoutePartsService = (function () {
    function RoutePartsService(router) {
        this.router = router;
    }
    RoutePartsService.prototype.ngOnInit = function () {
    };
    RoutePartsService.prototype.generateRouteParts = function (snapshot) {
        var routeParts = [];
        if (snapshot) {
            if (snapshot.firstChild) {
                routeParts = routeParts.concat(this.generateRouteParts(snapshot.firstChild));
            }
            if (snapshot.data['title'] && snapshot.url.length) {
                // console.log(snapshot.data['title'], snapshot.url)
                routeParts.push({
                    title: snapshot.data['title'],
                    breadcrumb: snapshot.data['breadcrumb'],
                    url: snapshot.url[0].path,
                    urlSegments: snapshot.url,
                    params: snapshot.params
                });
            }
        }
        return routeParts;
    };
    return RoutePartsService;
}());
RoutePartsService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object])
], RoutePartsService);

var _a;
//# sourceMappingURL=route-parts.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/theme/theme.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThemeService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__ = __webpack_require__("../../../../../src/app/helpers/dom.helper.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ThemeService = (function () {
    function ThemeService() {
        this.egretThemes = [{
                name: 'egret-dark-purple',
                baseColor: '#9c27b0',
                isActive: false
            }, {
                name: 'egret-dark-pink',
                baseColor: '#e91e63',
                isActive: false
            }, {
                name: 'egret-blue',
                baseColor: '#247ba0',
                isActive: false
            }, {
                name: 'egret-indigo',
                baseColor: '#3f51b5',
                isActive: true
            }];
        this.changeTheme({ name: 'egret-indigo' });
    }
    ThemeService.prototype.changeTheme = function (theme) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_1__helpers_dom_helper__["b" /* changeTheme */](this.egretThemes, theme.name);
        this.egretThemes.forEach(function (t) {
            t.isActive = false;
            if (t.name === theme.name) {
                t.isActive = true;
                _this.activatedThemeName = theme.name;
            }
        });
    };
    return ThemeService;
}());
ThemeService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], ThemeService);

//# sourceMappingURL=theme.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_material_prebuilt_themes_indigo_pink_css__ = __webpack_require__("../../../material/prebuilt-themes/indigo-pink.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_material_prebuilt_themes_indigo_pink_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__node_modules_angular_material_prebuilt_themes_indigo_pink_css__);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);

//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map