webpackJsonp(["profile.module"],{

/***/ "../../../../../src/app/views/profile/profile-blank/profile-blank.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/profile/profile-blank/profile-blank.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  profile-blank works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/profile/profile-blank/profile-blank.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileBlankComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileBlankComponent = (function () {
    function ProfileBlankComponent() {
    }
    ProfileBlankComponent.prototype.ngOnInit = function () {
    };
    return ProfileBlankComponent;
}());
ProfileBlankComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-profile-blank',
        template: __webpack_require__("../../../../../src/app/views/profile/profile-blank/profile-blank.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/profile/profile-blank/profile-blank.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ProfileBlankComponent);

//# sourceMappingURL=profile-blank.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/profile/profile-overview/profile-overview.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/profile/profile-overview/profile-overview.component.html":
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n  <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-mat=\"50\">\n    <!-- Time line -->\n    <mat-card class=\"default\">\n      <mat-card-title>Timeline</mat-card-title>\n      <mat-card-content>\n        <div class=\"timeline\">\n          <div class=\"timeline-item\">\n            <div class=\"timeline-badge\">\n              <img src=\"assets/images/face-6.jpg\" alt=\"\">\n            </div>\n            <div class=\"timeline-body\">\n              <div class=\"timeline-body-top\" fxLayout=\"row\">\n                <a href=\"#\" class=\"timeline-body-title mr-1\"><b>Jhone Doe</b> updated his status</a>\n                <span class=\"text-muted\">1 hour ago</span>\n                <span fxFlex></span>\n              </div>\n              <div class=\"timeline-body-content\">\n                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem necessitatibus natus fugit porro at sunt mollitia repellendus deserunt, libero similique.</p>\n              </div>\n              <div class=\"timeline-body-actions\">\n                <a href=\"#\" class=\"mr-1 text-muted\">Like</a>\n                <a href=\"#\" class=\"text-muted\">Comment</a>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"timeline-item\">\n            <div class=\"timeline-badge\">\n              <img src=\"assets/images/face-2.jpg\" alt=\"\">\n            </div>\n            <div class=\"timeline-body\">\n              <div class=\"timeline-body-top\" fxLayout=\"row\">\n                <a href=\"#\" class=\"timeline-body-title mr-1\"><b>Henry krick</b> added a new photo</a>\n                <span class=\"text-muted\">15 hours ago</span>\n                <span fxFlex></span>\n              </div>\n              <div class=\"timeline-body-content\">\n                <img src=\"assets/images/photo-600_220.jpg\" alt=\"\">\n              </div>\n              <div class=\"timeline-body-actions\">\n                <a href=\"#\" class=\"mr-1 text-muted\">Like</a>\n                <a href=\"#\" class=\"text-muted\">Comment</a>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"timeline-item\">\n            <div class=\"timeline-badge\">\n              <mat-icon class=\"icon-badge mat-bg-primary\">settings</mat-icon>\n            </div>\n            <div class=\"timeline-body\">\n              <div class=\"timeline-body-top\" fxLayout=\"row\">\n                <a href=\"#\" class=\"timeline-body-title mr-1\"><b>New follower</b></a>\n                <span class=\"text-muted\">2 days ago</span>\n                <span fxFlex></span>\n              </div>\n              <div class=\"timeline-body-content\">\n                <p><a class=\"mat-color-primary\" href=\"\">Kevin Huyn</a> and 34 others followed you.</p>\n              </div>\n              <div class=\"timeline-body-actions\">\n                <a href=\"#\" class=\"mr-1 text-muted\">Like</a>\n                <a href=\"#\" class=\"text-muted\">Comment</a>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"timeline-item\">\n            <div class=\"timeline-badge\">\n              <mat-icon class=\"icon-badge mat-bg-warn\">settings</mat-icon>\n            </div>\n            <div class=\"timeline-body\">\n              <div class=\"timeline-body-top\" fxLayout=\"row\">\n                <a href=\"#\" class=\"timeline-body-title mr-1\"><b>Membership upgraded</b></a>\n                <span class=\"text-muted\">5 days ago</span>\n                <span fxFlex></span>\n              </div>\n              <div class=\"timeline-body-content\">\n                <p>Membership has been upgraded to pro</p>\n              </div>\n              <div class=\"timeline-body-actions\">\n                <a href=\"#\" class=\"mr-1 text-muted\">Like</a>\n                <a href=\"#\" class=\"text-muted\">Comment</a>\n              </div>\n            </div>\n          </div>\n        </div>\n      </mat-card-content>\n    </mat-card>\n    <!-- End Time line -->\n\n    <mat-card class=\"default\">\n      <mat-card-title>Uploaded Photos</mat-card-title>\n      <mat-card-content class=\"p-0\">\n          <mat-grid-list cols=\"3\" rowHeight=\"1:1\" class=\"app-gallery\">\n              <!-- Gallery item -->\n              <mat-grid-tile *ngFor=\"let photo of photos\">\n                <img [src]=\"photo.url\" alt=\"\">\n                <!-- item detail, show on hover -->\n                <div class=\"gallery-control-wrap\">\n                  <div class=\"gallery-control\">\n                    <h4 class=\"photo-detail fz-1\" [fxHide.lt-sm]=\"true\">{{photo.name}}</h4>\n                    <span fxFlex></span>\n                    <button mat-icon-button [matMenuTriggerFor]=\"photoMenu\" class=\"\">\n                      <mat-icon>more_vert</mat-icon>\n                    </button>\n                    <mat-menu #photoMenu=\"matMenu\">\n                      <button mat-menu-item><mat-icon>send</mat-icon>Send as attachment</button>\n                      <button mat-menu-item><mat-icon>favorite</mat-icon>Favorite</button>\n                      <button mat-menu-item><mat-icon>delete</mat-icon>Delete</button>\n                    </mat-menu>\n                  </div>\n                </div>\n              </mat-grid-tile>\n            </mat-grid-list>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  \n  <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-mat=\"50\">\n    <mat-card class=\"default\">\n      <mat-card-title>Support Tickets</mat-card-title>\n      <mat-card-content class=\"p-0\">\n        <mat-list class=\"compact-list mb-1\">\n          <mat-list-item class=\"\" *ngFor=\"let t of tickets\">\n            <img mat-list-avatar class=\"mr-1\" [src]=\"t.img\" alt=\"\">\n            <div fxLayout=\"row\" fxFlex=\"100\">\n              <h6 class=\"m-0 mr-1\">{{t.name}}</h6>\n              <span fxFlex></span>\n              <div fxFlex=\"40\">{{t.text | excerpt:20 }}</div>\n              <span fxFlex></span>\n              <small class=\"text-muted mr-1\">{{ t.date | relativeTime}}</small>\n            </div>\n            <mat-chip mat-sm-chip [color]=\"'warn'\" [selected]=\"t.isOpen\">{{t.isOpen ? 'active' : 'closed'}}</mat-chip>\n          </mat-list-item>\n        </mat-list>\n        <div class=\"text-center\">\n          <button mat-button class=\"full-width\">View all</button>\n        </div>\n      </mat-card-content>\n    </mat-card>\n\n    <mat-card class=\"default\">\n      <mat-card-title>Assigned Tasks</mat-card-title>\n      <mat-card-content class=\"p-0\">\n        <div class=\"list-tasktype\">\n          <div class=\"tasktype-item\" *ngFor=\"let t of tasks\">\n            <mat-checkbox class=\"mr-1\"></mat-checkbox>\n            <span>{{t.text}}</span>\n            <span fxFlex></span>\n            <mat-chip mat-sm-chip color=\"primary\" [selected]=\"t.status ? true : false\">{{t.status ? 'completed' : 'pending'}}</mat-chip>\n            <button mat-icon-button mat-sm-button [matMenuTriggerFor]=\"taskMenu\" class=\"tasktype-action ml-1\">\n                <mat-icon class=\"text-muted\">more_vert</mat-icon>\n            </button>\n            <mat-menu #taskMenu=\"matMenu\">\n              <button mat-menu-item>\n                <mat-icon>done</mat-icon>\n                <span>Completed</span>\n              </button>\n              <button mat-menu-item>\n                <mat-icon>edit</mat-icon>\n                <span>Edit</span>\n              </button>\n              <button mat-menu-item>\n                <mat-icon>delete</mat-icon>\n                <span>Delete</span>\n              </button>\n            </mat-menu>\n          </div>\n        </div>\n        <div class=\"text-center\">\n          <button mat-button class=\"full-width\">View all</button>\n        </div>\n      </mat-card-content>\n    </mat-card>\n\n    <mat-card class=\"default\">\n      <mat-card-title>Activity</mat-card-title>\n      <mat-card-content class=\"p-0\" fxLayout=\"column\">\n        <div class=\"activity-data\">\n          <ngx-datatable class=\"material bg-white\" \n          [columnMode]=\"'force'\"  \n          [headerHeight]=\"50\"\n          [footerHeight]=\"50\" \n          [rowHeight]=\"50\"\n          [limit]=\"4\"\n          [rows]=\"activityData\">\n            <ngx-datatable-column name=\"Month\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.month }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Spent\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.spent }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Opened\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.opened }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Closed\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.closed }}\n              </ng-template>\n            </ngx-datatable-column>\n          </ngx-datatable>\n        </div>\n      </mat-card-content>\n    </mat-card>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/views/profile/profile-overview/profile-overview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileOverviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileOverviewComponent = (function () {
    function ProfileOverviewComponent() {
        this.activityData = [{
                month: 'January',
                spent: 240,
                opened: 8,
                closed: 30
            }, {
                month: 'February',
                spent: 140,
                opened: 6,
                closed: 20
            }, {
                month: 'March',
                spent: 220,
                opened: 10,
                closed: 20
            }, {
                month: 'April',
                spent: 440,
                opened: 40,
                closed: 60
            }, {
                month: 'May',
                spent: 340,
                opened: 40,
                closed: 60
            }];
        this.tasks = [{
                text: 'Lorem, ipsum dolor sit amet',
                status: 0
            }, {
                text: 'Lorem, ipsum dolor sit amet',
                status: 0
            }, {
                text: 'Lorem, ipsum dolor sit amet',
                status: 1
            }, {
                text: 'Lorem, ipsum dolor sit amet',
                status: 1
            }, {
                text: 'Lorem, ipsum dolor sit amet',
                status: 1
            }];
        this.tickets = [{
                img: 'assets/images/face-1.jpg',
                name: 'Mike Dake',
                text: 'Excerpt pipe is used.',
                date: new Date('07/12/2017'),
                isOpen: true
            }, {
                img: 'assets/images/face-5.jpg',
                name: 'Jhone Doe',
                text: 'My dashboard is not working.',
                date: new Date('07/7/2017'),
                isOpen: false
            }, {
                img: 'assets/images/face-3.jpg',
                name: 'Jhonson lee',
                text: 'Fix stock issue',
                date: new Date('04/10/2017'),
                isOpen: false
            }, {
                img: 'assets/images/face-4.jpg',
                name: 'Mikie Jyni',
                text: 'Renew my subscription.',
                date: new Date('07/7/2017'),
                isOpen: false
            }, {
                img: 'assets/images/face-5.jpg',
                name: 'Jhone Dake',
                text: 'Payment confirmation.',
                date: new Date('04/10/2017'),
                isOpen: false
            }];
        this.photos = [{
                name: 'Photo 1',
                url: 'assets/images/sq-15.jpg'
            }, {
                name: 'Photo 2',
                url: 'assets/images/sq-8.jpg'
            }, {
                name: 'Photo 3',
                url: 'assets/images/sq-9.jpg'
            }, {
                name: 'Photo 4',
                url: 'assets/images/sq-10.jpg'
            }, {
                name: 'Photo 5',
                url: 'assets/images/sq-11.jpg'
            }, {
                name: 'Photo 6',
                url: 'assets/images/sq-12.jpg'
            }];
    }
    ProfileOverviewComponent.prototype.ngOnInit = function () {
    };
    return ProfileOverviewComponent;
}());
ProfileOverviewComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-profile-overview',
        template: __webpack_require__("../../../../../src/app/views/profile/profile-overview/profile-overview.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/profile/profile-overview/profile-overview.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ProfileOverviewComponent);

//# sourceMappingURL=profile-overview.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/profile/profile-settings/profile-settings.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/profile/profile-settings/profile-settings.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"p-0\">\n  <mat-tab-group>\n    <mat-tab label=\"Account Settings\">\n      <mat-card-content class=\"mt-1\">\n          <form>\n            <mat-form-field class=\"full-width\">\n              <input\n              matInput\n              name=\"name\"\n              placeholder=\"Name\"\n              value=\"\">\n            </mat-form-field>\n            <mat-form-field class=\"full-width\">\n                <input\n                matInput\n                name=\"title\"\n                placeholder=\"Title\"\n                value=\"\">\n              </mat-form-field>\n            <mat-form-field class=\"full-width\">\n              <input\n              matInput\n              name=\"email\"\n              placeholder=\"Email\"\n              value=\"\">\n            </mat-form-field>\n            <mat-form-field class=\"full-width\">\n              <input\n              matInput\n              name=\"phone\"\n              placeholder=\"Phone\"\n              value=\"\">\n            </mat-form-field>\n            <mat-form-field class=\"full-width\">\n              <input\n              matInput\n              name=\"address\"\n              placeholder=\"Adresss\"\n              value=\"\">\n            </mat-form-field>\n            <mat-form-field class=\"full-width\">\n              <input\n              matInput\n              name=\"website\"\n              placeholder=\"Website\"\n              value=\"\">\n            </mat-form-field>\n            <button mat-raised-button color=\"primary\">Save</button>\n          </form>\n      </mat-card-content>\n    </mat-tab>\n    <mat-tab label=\"Profile Picture\">\n      <mat-card-content>\n        <div class=\"mb-1 mt-1\">\n          <p>Upload a profile picture</p>\n          <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" />\n        </div>\n\n        <div class=\"mb-1\">\n          <div ng2FileDrop\n          [ngClass]=\"{'dz-file-over': hasBaseDropZoneOver}\"\n          [uploader]=\"uploader\"\n          (fileOver)=\"fileOverBase($event)\"\n          class=\"fileupload-drop-zone\">\n          Drop png/jpeg file here\n          </div>\n        </div>\n        <table class=\"default-table mat-box-shadow\" style=\"width: 100%\">\n          <thead>\n            <tr>\n                <th width=\"30%\">Name</th>\n                <th>Size</th>\n                <th>Progress</th>\n                <th>Status</th>\n                <th>Actions</th>\n            </tr>\n            </thead>\n            <tbody *ngIf=\"uploader.queue.length; else tableNoData\">\n            <tr *ngFor=\"let item of uploader.queue\">\n                <td>{{ item?.file?.name }}</td>\n                <td nowrap>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\n                <td>\n                    <div class=\"progress\" style=\"margin-bottom: 0;\">\n                        <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\n                        <mat-progress-bar\n                          class=\"\"\n                          color=\"primary\"\n                          mode=\"determinate\"\n                          [value]=\"item.progress\">\n                        </mat-progress-bar>\n                    </div>\n                </td>\n                <td class=\"\">\n                    <mat-icon *ngIf=\"item.isSuccess\">check</mat-icon>\n                    <mat-icon *ngIf=\"item.isCancel\" color=\"accent\">cancel</mat-icon>\n                    <mat-icon *ngIf=\"item.isError\" color=\"warn\">error</mat-icon>\n                </td>\n                <td nowrap>\n                    <button \n                    mat-raised-button\n                    class=\"mat-primary\"\n                    (click)=\"item.upload()\"\n                    [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">Upload</button>\n                    <button \n                    mat-raised-button\n                    class=\"mat-accent\"\n                    (click)=\"item.cancel()\"\n                    [disabled]=\"!item.isUploading\">Cancel</button>\n\n                    <button \n                    mat-raised-button\n                    class=\"mat-warn\"\n                    (click)=\"item.remove()\">Remove</button>\n                </td>\n            </tr>\n            </tbody>\n            <ng-template #tableNoData>\n              <p [ngStyle]=\"{padding: '0 1.2rem'}\">Queue is empty</p>\n            </ng-template>\n        </table>\n      </mat-card-content>\n    </mat-tab>\n    <mat-tab label=\"Privacy Settings\">\n      <mat-card-content>\n        <div class=\"mb-1 mt-1\">\n          <mat-checkbox> Get weekly news in your email.</mat-checkbox>\n        </div>\n        <div class=\"mb-1\">\n          <mat-checkbox> Get notification when someone follows you.</mat-checkbox>\n        </div>\n        <div class=\"mb-1\">\n          <mat-checkbox> Get email when someone follows you.</mat-checkbox>\n        </div>\n        <div class=\"mb-1\">\n            <p>Choose your admin panel color schemes.</p>\n            <mat-radio-group fxLayout=\"column\" fxLayoutGap=\"4px\">\n              <mat-radio-button value=\"indigo\">Indigo</mat-radio-button>\n              <mat-radio-button value=\"blue\">Blue</mat-radio-button>\n              <mat-radio-button value=\"pink\">Pink</mat-radio-button>\n              <mat-radio-button value=\"purple\">Purple</mat-radio-button>\n            </mat-radio-group>\n        </div>\n        <button mat-raised-button color=\"primary\">Save</button>\n      </mat-card-content>\n    </mat-tab>\n  </mat-tab-group>\n</mat-card>"

/***/ }),

/***/ "../../../../../src/app/views/profile/profile-settings/profile-settings.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileSettingsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfileSettingsComponent = (function () {
    function ProfileSettingsComponent() {
        this.uploader = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"]({ url: 'upload_url' });
        this.hasBaseDropZoneOver = false;
    }
    ProfileSettingsComponent.prototype.ngOnInit = function () {
    };
    ProfileSettingsComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    return ProfileSettingsComponent;
}());
ProfileSettingsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-profile-settings',
        template: __webpack_require__("../../../../../src/app/views/profile/profile-settings/profile-settings.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/profile/profile-settings/profile-settings.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ProfileSettingsComponent);

//# sourceMappingURL=profile-settings.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n  <div fxFlex=\"100\" fxFlex.gt-mat=\"300px\" fxFlex.gt-sm=\"50\">\r\n    <mat-card class=\"profile-sidebar mb-1 pb-0\">\r\n      <div class=\"propic text-center\">\r\n        <img src=\"assets/images/sq-face-220.jpg\" alt=\"\">\r\n      </div>\r\n      <div class=\"profile-title text-center mb-1\">\r\n        <div class=\"main-title\">Jhone <b>Doe</b></div>\r\n        <div class=\"subtitle mb-05\">Big Data Expert</div>\r\n        <div class=\"text-muted\">Hi I'm Jhone, sit amet consectetur adipisicing elit. Aperiam repellendus nam perspiciatis.</div>\r\n      </div>\r\n      <div class=\"profile-actions text-center mb-1\">\r\n        <button mat-raised-button color=\"primary\">Message</button>\r\n        <button mat-raised-button color=\"accent\">Follow</button>\r\n      </div>\r\n      <div class=\"profile-nav\">\r\n        <mat-nav-list>\r\n          <mat-list-item routerLink=\"/profile/overview\" routerLinkActive=\"list-item-active\">\r\n            <mat-icon>home</mat-icon>\r\n            Overview\r\n          </mat-list-item>\r\n          <mat-divider></mat-divider>\r\n          <mat-list-item routerLink=\"/profile/settings\" routerLinkActive=\"list-item-active\">\r\n            <mat-icon>settings</mat-icon>\r\n            Settings\r\n          </mat-list-item>\r\n          <mat-divider></mat-divider>\r\n          <mat-list-item routerLink=\"/profile/blank\" routerLinkActive=\"list-item-active\">\r\n            <mat-icon>content_paste</mat-icon>\r\n            Blank\r\n          </mat-list-item>\r\n        </mat-nav-list>\r\n      </div>\r\n    </mat-card>\r\n\r\n    <!-- Chart grid -->\r\n    <mat-card class=\"default\">\r\n      <mat-card-title>Summary</mat-card-title>\r\n      <mat-card-content>\r\n          <div fxLayout=\"row\" fxLayoutWrap=\"wrap\" class=\"doughnut-grid text-center\">\r\n              <div fxFlex=\"50\" class=\"doughnut-grid-item mat-bg-primary\">\r\n                  <canvas \r\n                  height=\"120\"\r\n                  baseChart\r\n                  class=\"chart\"\r\n                  [data]=\"doughnutChartData1\"\r\n                  [options]=\"doughnutOptions\"\r\n                  [colors]=\"doughnutChartColors\"\r\n                  [chartType]=\"doughnutChartType\"></canvas>\r\n                  <small>Space: {{data1}}/{{total1}} GB</small>\r\n            </div>\r\n            <div fxFlex=\"50\" class=\"doughnut-grid-item light-gray\">\r\n                <canvas \r\n                height=\"120\" \r\n                baseChart \r\n                class=\"chart\"\r\n                [data]=\"doughnutChartData2\"\r\n                [options]=\"doughnutOptions\"\r\n                [colors]=\"doughnutChartColors\"\r\n                [chartType]=\"doughnutChartType\"></canvas>\r\n                <small>Sales: 450</small>\r\n          </div>\r\n          <div fxFlex=\"50\" class=\"doughnut-grid-item light-gray\">\r\n              <canvas \r\n              height=\"120\" \r\n              baseChart \r\n              class=\"chart\"\r\n              [data]=\"doughnutChartData2\"\r\n              [options]=\"doughnutOptions\"\r\n              [colors]=\"doughnutChartColors\"\r\n              [chartType]=\"doughnutChartType\"></canvas>\r\n              <small>Spent: $500</small>\r\n        </div>\r\n        <div fxFlex=\"50\" class=\"doughnut-grid-item mat-bg-primary\">\r\n            <canvas \r\n            height=\"120\" \r\n            baseChart \r\n            class=\"chart\"\r\n            [data]=\"doughnutChartData2\"\r\n            [options]=\"doughnutOptions\"\r\n            [colors]=\"doughnutChartColors\"\r\n            [chartType]=\"doughnutChartType\"></canvas>\r\n            <small>Follower: 2000</small>\r\n      </div>\r\n        </div>\r\n      </mat-card-content>\r\n    </mat-card>\r\n    <!-- Contact Information -->\r\n    <mat-card class=\"default\">\r\n      <mat-card-title>Contact Information</mat-card-title>\r\n      <mat-card-content class=\"pt-0\">\r\n        <mat-list>\r\n          <mat-list-item><mat-icon class=\"mr-1\">public</mat-icon> www.mhrafi.com</mat-list-item>\r\n          <mat-list-item><mat-icon class=\"mr-1\">email</mat-icon> example@gmail.com</mat-list-item>\r\n          <mat-list-item><mat-icon class=\"mr-1\">phone</mat-icon> 8801822778800</mat-list-item>\r\n          <mat-list-item><mat-icon class=\"mr-1\">add_location</mat-icon> SUST, Sylhet, BD</mat-list-item>\r\n        </mat-list>\r\n      </mat-card-content>\r\n    </mat-card>\r\n  </div>\r\n\r\n<!-- Profile Views -->\r\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"50\" fxFlex.gt-mat=\"calc(100% - 300px)\">\r\n    <router-outlet></router-outlet>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfileComponent = (function () {
    function ProfileComponent(router) {
        this.router = router;
        this.activeView = 'overview';
        // Doughnut
        this.doughnutChartColors = [{
                backgroundColor: ['#fff', 'rgba(0, 0, 0, .24)',]
            }];
        this.total1 = 500;
        this.data1 = 200;
        this.doughnutChartData1 = [this.data1, (this.total1 - this.data1)];
        this.total2 = 1000;
        this.data2 = 400;
        this.doughnutChartData2 = [this.data2, (this.total2 - this.data2)];
        this.doughnutChartType = 'doughnut';
        this.doughnutOptions = {
            cutoutPercentage: 85,
            responsive: true,
            legend: {
                display: false,
                position: 'bottom'
            },
            elements: {
                arc: {
                    borderWidth: 0,
                }
            },
            tooltips: {
                enabled: false
            }
        };
    }
    ProfileComponent.prototype.ngOnInit = function () {
        this.activeView = this.router.snapshot.params['view'];
    };
    return ProfileComponent;
}());
ProfileComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-profile',
        template: __webpack_require__("../../../../../src/app/views/profile/profile.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object])
], ProfileComponent);

var _a;
//# sourceMappingURL=profile.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/profile/profile.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileModule", function() { return ProfileModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__ = __webpack_require__("../../../flex-layout/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_charts_ng2_charts__ = __webpack_require__("../../../../ng2-charts/ng2-charts.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_charts_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_charts_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pipes_common_common_pipes_module__ = __webpack_require__("../../../../../src/app/pipes/common/common-pipes.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__profile_component__ = __webpack_require__("../../../../../src/app/views/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__profile_overview_profile_overview_component__ = __webpack_require__("../../../../../src/app/views/profile/profile-overview/profile-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__profile_settings_profile_settings_component__ = __webpack_require__("../../../../../src/app/views/profile/profile-settings/profile-settings.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__profile_blank_profile_blank_component__ = __webpack_require__("../../../../../src/app/views/profile/profile-blank/profile-blank.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__profile_routing__ = __webpack_require__("../../../../../src/app/views/profile/profile.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var ProfileModule = (function () {
    function ProfileModule() {
    }
    return ProfileModule;
}());
ProfileModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["p" /* MatListModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["n" /* MatIconModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["c" /* MatButtonModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["e" /* MatCardModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["q" /* MatMenuModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["C" /* MatSlideToggleModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["m" /* MatGridListModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["g" /* MatChipsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MatCheckboxModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["x" /* MatRadioModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["I" /* MatTabsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["o" /* MatInputModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["v" /* MatProgressBarModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__["a" /* FlexLayoutModule */],
            __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_datatable__["NgxDatatableModule"],
            __WEBPACK_IMPORTED_MODULE_7_ng2_charts_ng2_charts__["ChartsModule"],
            __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload_ng2_file_upload__["FileUploadModule"],
            __WEBPACK_IMPORTED_MODULE_9__pipes_common_common_pipes_module__["a" /* CommonPipesModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_14__profile_routing__["a" /* ProfileRoutes */])
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_10__profile_component__["a" /* ProfileComponent */], __WEBPACK_IMPORTED_MODULE_11__profile_overview_profile_overview_component__["a" /* ProfileOverviewComponent */], __WEBPACK_IMPORTED_MODULE_12__profile_settings_profile_settings_component__["a" /* ProfileSettingsComponent */], __WEBPACK_IMPORTED_MODULE_13__profile_blank_profile_blank_component__["a" /* ProfileBlankComponent */]]
    })
], ProfileModule);

//# sourceMappingURL=profile.module.js.map

/***/ }),

/***/ "../../../../../src/app/views/profile/profile.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__profile_component__ = __webpack_require__("../../../../../src/app/views/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__profile_overview_profile_overview_component__ = __webpack_require__("../../../../../src/app/views/profile/profile-overview/profile-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_settings_profile_settings_component__ = __webpack_require__("../../../../../src/app/views/profile/profile-settings/profile-settings.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_blank_profile_blank_component__ = __webpack_require__("../../../../../src/app/views/profile/profile-blank/profile-blank.component.ts");




var ProfileRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__profile_component__["a" /* ProfileComponent */],
        children: [{
                path: 'overview',
                component: __WEBPACK_IMPORTED_MODULE_1__profile_overview_profile_overview_component__["a" /* ProfileOverviewComponent */],
                data: { title: 'Overview', breadcrumb: 'OVERVIEW' }
            },
            {
                path: 'settings',
                component: __WEBPACK_IMPORTED_MODULE_2__profile_settings_profile_settings_component__["a" /* ProfileSettingsComponent */],
                data: { title: 'Settings', breadcrumb: 'SETTINGS' }
            },
            {
                path: 'blank',
                component: __WEBPACK_IMPORTED_MODULE_3__profile_blank_profile_blank_component__["a" /* ProfileBlankComponent */],
                data: { title: 'Blank', breadcrumb: 'BLANK' }
            }]
    }
];
//# sourceMappingURL=profile.routing.js.map

/***/ })

});
//# sourceMappingURL=profile.module.chunk.js.map