webpackJsonp(["others.module"],{

/***/ "../../../../../src/app/views/others/app-blank/app-blank.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/others/app-blank/app-blank.component.html":
/***/ (function(module, exports) {

module.exports = "<p class=\"m-333\">\r\n  This is a blank component.\r\n</p>\r\n"

/***/ }),

/***/ "../../../../../src/app/views/others/app-blank/app-blank.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppBlankComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppBlankComponent = (function () {
    function AppBlankComponent() {
    }
    AppBlankComponent.prototype.ngOnInit = function () {
    };
    return AppBlankComponent;
}());
AppBlankComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-blank',
        template: __webpack_require__("../../../../../src/app/views/others/app-blank/app-blank.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/others/app-blank/app-blank.component.css")]
    }),
    __metadata("design:paramtypes", [])
], AppBlankComponent);

//# sourceMappingURL=app-blank.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/others/app-gallery/app-gallery.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/others/app-gallery/app-gallery.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"p-0\">\r\n  <mat-card-title class=\"m-0\">\r\n    <div class=\"card-title-text\">Media gallery</div>\r\n    <mat-divider></mat-divider>\r\n  </mat-card-title>\r\n  <mat-card-content class=\"p-0\">\r\n    <mat-grid-list cols=\"3\" rowHeight=\"1:1\" class=\"app-gallery\">\r\n      <!-- Gallery item -->\r\n      <mat-grid-tile *ngFor=\"let photo of photos\">\r\n        <img [src]=\"photo.url\" alt=\"\">\r\n        <!-- item detail, show on hover -->\r\n        <div class=\"gallery-control-wrap\">\r\n          <div class=\"gallery-control\">\r\n            <h4 class=\"photo-detail fz-1\" [fxHide.lt-sm]=\"true\">{{photo.name}}</h4>\r\n            <span fxFlex></span>\r\n            <button mat-icon-button [matMenuTriggerFor]=\"photoMenu\" class=\"\">\r\n                <mat-icon>more_vert</mat-icon>\r\n            </button>\r\n            <mat-menu #photoMenu=\"matMenu\">\r\n                <button mat-menu-item><mat-icon>send</mat-icon>Send as attachment</button>\r\n                <button mat-menu-item><mat-icon>favorite</mat-icon>Favorite</button>\r\n                <button mat-menu-item><mat-icon>delete</mat-icon>Delete</button>\r\n            </mat-menu>\r\n          </div>\r\n        </div>\r\n      </mat-grid-tile>\r\n    </mat-grid-list>\r\n  </mat-card-content>\r\n</mat-card>\r\n"

/***/ }),

/***/ "../../../../../src/app/views/others/app-gallery/app-gallery.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppGalleryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppGalleryComponent = (function () {
    function AppGalleryComponent() {
        this.photos = [{
                name: 'Photo 1',
                url: 'assets/images/sq-14.jpg'
            }, {
                name: 'Photo 2',
                url: 'assets/images/sq-16.jpg'
            }, {
                name: 'Photo 3',
                url: 'assets/images/sq-15.jpg'
            }, {
                name: 'Photo 4',
                url: 'assets/images/sq-17.jpg'
            }, {
                name: 'Photo 5',
                url: 'assets/images/sq-13.jpg'
            }, {
                name: 'Photo 6',
                url: 'assets/images/sq-12.jpg'
            }, {
                name: 'Photo 7',
                url: 'assets/images/sq-11.jpg'
            }, {
                name: 'Photo 8',
                url: 'assets/images/sq-10.jpg'
            }];
    }
    AppGalleryComponent.prototype.ngOnInit = function () {
    };
    return AppGalleryComponent;
}());
AppGalleryComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-gallery',
        template: __webpack_require__("../../../../../src/app/views/others/app-gallery/app-gallery.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/others/app-gallery/app-gallery.component.css")]
    }),
    __metadata("design:paramtypes", [])
], AppGalleryComponent);

//# sourceMappingURL=app-gallery.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/others/app-pricing/app-pricing.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/others/app-pricing/app-pricing.component.html":
/***/ (function(module, exports) {

module.exports = "<md-card class=\"p-0\">\r\n  <md-card-title class=\"\">\r\n    <div class=\"card-title-text\">Plans and Pricing</div>\r\n    <md-divider></md-divider>\r\n  </md-card-title>\r\n  <md-card-content>\r\n    <md-slide-toggle [(ngModel)]=\"isAnnualSelected\" color=\"primary\" class=\"mb-1\">Get upto 10% discount annually</md-slide-toggle>\r\n\r\n    <div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n      <!-- Pricing box -->\r\n      <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\">\r\n        <md-card class=\"plan-pricing text-center p-0\">\r\n          <md-card-title class=\"light-gray\">\r\n            <div class=\"card-title-text\">\r\n              <div class=\"\">Developer</div>\r\n              <div class=\"text-sm text-muted\">For New Developers</div>\r\n            </div>\r\n            <md-divider></md-divider>\r\n          </md-card-title>\r\n          <md-card-content>\r\n            <h1><strong>FREE</strong></h1>\r\n            <md-list dense class=\"mb-1\">\r\n              <md-list-item>10GB of Bandwidth</md-list-item>\r\n              <md-list-item>Max 50 connection</md-list-item>\r\n              <md-list-item>512MB RAM</md-list-item>\r\n              <md-list-item class=\"text-muted\">Unlimited access</md-list-item>\r\n              <md-list-item class=\"text-muted\">Unlimited User</md-list-item>\r\n              <md-list-item class=\"text-muted\">Data analytics</md-list-item>\r\n            </md-list>\r\n            <button md-raised-button class=\"mat-accent\">Choose</button>\r\n          </md-card-content>\r\n        </md-card>\r\n      </div>\r\n      <!-- Pricing box -->\r\n      <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\">\r\n        <md-card class=\"plan-pricing text-center p-0\">\r\n          <md-card-title class=\"mat-bg-primary\">\r\n            <div class=\"card-title-text\">\r\n              <div class=\"\">Starter</div>\r\n              <div class=\"text-sm text-muted-white\">For Professional Developers</div>\r\n            </div>\r\n            <md-divider></md-divider>\r\n          </md-card-title>\r\n          <md-card-content>\r\n            <h1>\r\n              <strong>$ \r\n              <span *ngIf=\"!isAnnualSelected\">{{30}} /Mo</span>\r\n              <span *ngIf=\"isAnnualSelected\">{{30 * 12 * .9}} /Yr</span>\r\n              </strong>\r\n            </h1>\r\n            <md-list dense class=\"mb-1\">\r\n              <md-list-item>100GB of Bandwidth</md-list-item>\r\n              <md-list-item>Max 500 connection</md-list-item>\r\n              <md-list-item>1GB RAM</md-list-item>\r\n              <md-list-item>Unlimited access</md-list-item>\r\n              <md-list-item class=\"text-muted\">Unlimited User</md-list-item>\r\n              <md-list-item class=\"text-muted\">Data analytics</md-list-item>\r\n            </md-list>\r\n            <button md-raised-button class=\"mat-accent\">Choose</button>\r\n          </md-card-content>\r\n        </md-card>\r\n      </div>\r\n      <!-- Pricing box -->\r\n      <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\">\r\n        <md-card class=\"plan-pricing text-center p-0\">\r\n          <md-card-title class=\"light-gray\">\r\n            <div class=\"card-title-text\">\r\n              <div class=\"\">Business</div>\r\n              <div class=\"text-sm text-muted\">For Small Businesses</div>\r\n            </div>\r\n            <md-divider></md-divider>\r\n          </md-card-title>\r\n          <md-card-content>\r\n            <h1>\r\n              <strong>$ \r\n              <span *ngIf=\"!isAnnualSelected\">{{60}} /Mo</span>\r\n              <span *ngIf=\"isAnnualSelected\">{{60 * 12 * .9}} /Yr</span>\r\n              </strong>\r\n            </h1>\r\n            <md-list dense class=\"mb-1\">\r\n              <md-list-item>100GB of Bandwidth</md-list-item>\r\n              <md-list-item>Max 1500 connection</md-list-item>\r\n              <md-list-item>2GB RAM</md-list-item>\r\n              <md-list-item>Unlimited access</md-list-item>\r\n              <md-list-item>Unlimited User</md-list-item>\r\n              <md-list-item class=\"text-muted\">Data analytics</md-list-item>\r\n            </md-list>\r\n            <button md-raised-button class=\"mat-accent\">Choose</button>\r\n          </md-card-content>\r\n        </md-card>\r\n      </div>\r\n      <!-- Pricing box -->\r\n      <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\">\r\n        <md-card class=\"plan-pricing text-center p-0\">\r\n          <md-card-title class=\"light-gray\">\r\n            <div class=\"card-title-text\">\r\n              <div class=\"\">Enterprise</div>\r\n              <div class=\"text-sm text-muted\">For Large companies</div>\r\n            </div>\r\n            <md-divider></md-divider>\r\n          </md-card-title>\r\n          <md-card-content>\r\n            <h1>\r\n              <strong>$ \r\n              <span *ngIf=\"!isAnnualSelected\">{{160}} /Mo</span>\r\n              <span *ngIf=\"isAnnualSelected\">{{160 * 12 * .9}} /Yr</span>\r\n              </strong>\r\n            </h1>\r\n            <md-list dense class=\"mb-1\">\r\n              <md-list-item>1000GB of Bandwidth</md-list-item>\r\n              <md-list-item>Max 5000 connection</md-list-item>\r\n              <md-list-item>8GB RAM</md-list-item>\r\n              <md-list-item>Unlimited access</md-list-item>\r\n              <md-list-item>Unlimited User</md-list-item>\r\n              <md-list-item>Data analytics</md-list-item>\r\n            </md-list>\r\n            <button md-raised-button class=\"mat-accent\">Choose</button>\r\n          </md-card-content>\r\n        </md-card>\r\n      </div>\r\n\r\n    </div>\r\n  </md-card-content>\r\n</md-card>"

/***/ }),

/***/ "../../../../../src/app/views/others/app-pricing/app-pricing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPricingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppPricingComponent = (function () {
    function AppPricingComponent() {
        this.isAnnualSelected = false;
    }
    AppPricingComponent.prototype.ngOnInit = function () {
    };
    return AppPricingComponent;
}());
AppPricingComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-pricing',
        template: __webpack_require__("../../../../../src/app/views/others/app-pricing/app-pricing.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/others/app-pricing/app-pricing.component.css")]
    }),
    __metadata("design:paramtypes", [])
], AppPricingComponent);

//# sourceMappingURL=app-pricing.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/others/app-users/app-users.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/others/app-users/app-users.component.html":
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\" >\r\n  <div\r\n  *ngFor=\"let user of users\" \r\n  fxFlex=\"100\"\r\n  fxFlex.gt-sm=\"33\"\r\n  fxFlex.sm=\"50\">\r\n    <mat-card class=\"user-card p-0\">\r\n      <mat-card-title class=\"mat-bg-primary\">\r\n        <div class=\"card-title-text\">\r\n          <a href=\"\" class=\"toolbar-avatar mat mr-1\"><img [src]=\"user.photo\" alt=\"\"></a>\r\n          <span>{{user.name}}</span>\r\n          <span fxFlex></span>\r\n          <button mat-icon-button [matMenuTriggerFor]=\"userMenu\" class=\"\">\r\n              <mat-icon class=\"\">more_vert</mat-icon>\r\n          </button>\r\n          <mat-menu #userMenu=\"matMenu\">\r\n              <button mat-menu-item>Follow</button>\r\n              <button mat-menu-item>Message</button>\r\n              <button mat-menu-item>Block</button>\r\n              <button mat-menu-item>Delete</button>\r\n          </mat-menu>\r\n        </div>\r\n        <mat-divider></mat-divider>\r\n      </mat-card-title>\r\n      <mat-card-content>\r\n        <!-- user detail lines-->\r\n        <div class=\"user-details\">\r\n          <p><mat-icon class=\"text-muted\">card_membership</mat-icon>{{user.membership}}</p>\r\n          <p><mat-icon class=\"text-muted\">date_range</mat-icon>Member since {{user.registered | date}}</p>\r\n          <p><mat-icon class=\"text-muted\">phone</mat-icon>{{user.phone}}</p>\r\n          <p><mat-icon class=\"text-muted\">location_on</mat-icon>{{user.address}}</p>\r\n        </div>\r\n      </mat-card-content>\r\n    </mat-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/others/app-users/app-users.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppUsersComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppUsersComponent = (function () {
    function AppUsersComponent() {
        this.users = [
            {
                "name": "Snow Benton",
                "membership": "Paid Member",
                "phone": "+1 (956) 486-2327",
                "photo": "assets/images/face-1.jpg",
                "address": "329 Dictum Court, Minnesota",
                "registered": "2016-07-09"
            },
            {
                "name": "Kay Sellers",
                "membership": "Paid Member",
                "phone": "+1 (929) 406-3172",
                "photo": "assets/images/face-2.jpg",
                "address": "893 Garden Place, American Samoa",
                "registered": "2017-02-16"
            },
            {
                "name": "Robert Middleton",
                "membership": "Paid Member",
                "phone": "+1 (995) 451-2205",
                "photo": "assets/images/face-3.jpg",
                "address": "301 Hazel Court, West Virginia",
                "registered": "2017-01-22"
            },
            {
                "name": "Delaney Randall",
                "membership": "Paid Member",
                "phone": "+1 (922) 599-2410",
                "photo": "assets/images/face-4.jpg",
                "address": "128 Kensington Walk, Ohio",
                "registered": "2016-12-08"
            },
            {
                "name": "Melendez Lawrence",
                "membership": "Paid Member",
                "phone": "+1 (824) 589-2029",
                "photo": "assets/images/face-5.jpg",
                "address": "370 Lincoln Avenue, Florida",
                "registered": "2015-03-29"
            },
            {
                "name": "Galloway Fitzpatrick",
                "membership": "Paid Member",
                "phone": "+1 (907) 477-2375",
                "photo": "assets/images/face-6.jpg",
                "address": "296 Stuyvesant Avenue, Iowa",
                "registered": "2015-12-12"
            },
            {
                "name": "Watson Joyce",
                "membership": "Paid Member",
                "phone": "+1 (982) 500-3137",
                "photo": "assets/images/face-7.jpg",
                "address": "224 Visitation Place, Illinois",
                "registered": "2015-08-19"
            },
            {
                "name": "Ada Kidd",
                "membership": "Paid Member",
                "phone": "+1 (832) 531-2385",
                "photo": "assets/images/face-1.jpg",
                "address": "230 Oxford Street, South Dakota",
                "registered": "2016-08-11"
            },
            {
                "name": "Raquel Mcintyre",
                "membership": "Paid Member",
                "phone": "+1 (996) 443-2102",
                "photo": "assets/images/face-2.jpg",
                "address": "393 Sullivan Street, Palau",
                "registered": "2014-09-03"
            },
            {
                "name": "Juliette Hunter",
                "membership": "Paid Member",
                "phone": "+1 (876) 568-2964",
                "photo": "assets/images/face-3.jpg",
                "address": "191 Stryker Court, New Jersey",
                "registered": "2017-01-18"
            },
            {
                "name": "Workman Floyd",
                "membership": "Paid Member",
                "phone": "+1 (996) 481-2712",
                "photo": "assets/images/face-4.jpg",
                "address": "350 Imlay Street, Utah",
                "registered": "2017-05-01"
            },
            {
                "name": "Amanda Bean",
                "membership": "Paid Member",
                "phone": "+1 (894) 512-3907",
                "photo": "assets/images/face-5.jpg",
                "address": "254 Stockton Street, Vermont",
                "registered": "2014-08-30"
            }
        ];
    }
    AppUsersComponent.prototype.ngOnInit = function () {
    };
    return AppUsersComponent;
}());
AppUsersComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-users',
        template: __webpack_require__("../../../../../src/app/views/others/app-users/app-users.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/others/app-users/app-users.component.css")]
    }),
    __metadata("design:paramtypes", [])
], AppUsersComponent);

//# sourceMappingURL=app-users.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/others/others.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OthersModule", function() { return OthersModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__ = __webpack_require__("../../../flex-layout/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_charts_ng2_charts__ = __webpack_require__("../../../../ng2-charts/ng2-charts.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_charts_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_charts_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pipes_common_common_pipes_module__ = __webpack_require__("../../../../../src/app/pipes/common/common-pipes.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_gallery_app_gallery_component__ = __webpack_require__("../../../../../src/app/views/others/app-gallery/app-gallery.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_pricing_app_pricing_component__ = __webpack_require__("../../../../../src/app/views/others/app-pricing/app-pricing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_users_app_users_component__ = __webpack_require__("../../../../../src/app/views/others/app-users/app-users.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__app_blank_app_blank_component__ = __webpack_require__("../../../../../src/app/views/others/app-blank/app-blank.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__others_routing__ = __webpack_require__("../../../../../src/app/views/others/others.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var OthersModule = (function () {
    function OthersModule() {
    }
    return OthersModule;
}());
OthersModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["p" /* MatListModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["n" /* MatIconModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["c" /* MatButtonModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["e" /* MatCardModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["q" /* MatMenuModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["C" /* MatSlideToggleModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["m" /* MatGridListModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["g" /* MatChipsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MatCheckboxModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["x" /* MatRadioModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["I" /* MatTabsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["o" /* MatInputModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["v" /* MatProgressBarModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__["a" /* FlexLayoutModule */],
            __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_datatable__["NgxDatatableModule"],
            __WEBPACK_IMPORTED_MODULE_7_ng2_charts_ng2_charts__["ChartsModule"],
            __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload_ng2_file_upload__["FileUploadModule"],
            __WEBPACK_IMPORTED_MODULE_9__pipes_common_common_pipes_module__["a" /* CommonPipesModule */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_14__others_routing__["a" /* OthersRoutes */])
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_10__app_gallery_app_gallery_component__["a" /* AppGalleryComponent */], __WEBPACK_IMPORTED_MODULE_11__app_pricing_app_pricing_component__["a" /* AppPricingComponent */], __WEBPACK_IMPORTED_MODULE_12__app_users_app_users_component__["a" /* AppUsersComponent */], __WEBPACK_IMPORTED_MODULE_13__app_blank_app_blank_component__["a" /* AppBlankComponent */]]
    })
], OthersModule);

//# sourceMappingURL=others.module.js.map

/***/ }),

/***/ "../../../../../src/app/views/others/others.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OthersRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_gallery_app_gallery_component__ = __webpack_require__("../../../../../src/app/views/others/app-gallery/app-gallery.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_pricing_app_pricing_component__ = __webpack_require__("../../../../../src/app/views/others/app-pricing/app-pricing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_users_app_users_component__ = __webpack_require__("../../../../../src/app/views/others/app-users/app-users.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_blank_app_blank_component__ = __webpack_require__("../../../../../src/app/views/others/app-blank/app-blank.component.ts");




var OthersRoutes = [
    {
        path: '',
        children: [{
                path: 'gallery',
                component: __WEBPACK_IMPORTED_MODULE_0__app_gallery_app_gallery_component__["a" /* AppGalleryComponent */],
                data: { title: 'Gallery', breadcrumb: 'GALLERY' }
            }, {
                path: 'pricing',
                component: __WEBPACK_IMPORTED_MODULE_1__app_pricing_app_pricing_component__["a" /* AppPricingComponent */],
                data: { title: 'Pricing', breadcrumb: 'PRICINGS' }
            }, {
                path: 'users',
                component: __WEBPACK_IMPORTED_MODULE_2__app_users_app_users_component__["a" /* AppUsersComponent */],
                data: { title: 'Users', breadcrumb: 'USERS' }
            }, {
                path: 'blank',
                component: __WEBPACK_IMPORTED_MODULE_3__app_blank_app_blank_component__["a" /* AppBlankComponent */],
                data: { title: 'Blank', breadcrumb: 'BLANK' }
            }]
    }
];
//# sourceMappingURL=others.routing.js.map

/***/ })

});
//# sourceMappingURL=others.module.chunk.js.map