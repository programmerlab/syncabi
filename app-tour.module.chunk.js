webpackJsonp(["app-tour.module"],{

/***/ "../../../../../src/app/views/app-tour/app-tour.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/app-tour/app-tour.component.html":
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\" fxFlex.sm=\"50\">\n    <md-card class=\"default\">\n      <md-card-title>Example User Tour</md-card-title>\n      <md-card-subtitle>Click this button to start a demo Tour.</md-card-subtitle>\n      <md-card-content>\n        <button \n        color=\"primary\" \n        md-raised-button\n        (click)=\"startTour()\">Start Tour</button>\n      </md-card-content>\n    </md-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\" fxFlex.sm=\"50\">\n    <md-card class=\"default\" id=\"areaOne\">\n      <md-card-title class=\"m-0\">Define your steps</md-card-title>\n      <md-card-content>\n        <pre>\n        <code [style.display]=\"'block'\">steps = {{\n        {\n          id: 'demo-tour', \n          showPrevButton: true, \n          steps: [\n            {\n              title: 'Step one',\n              content: 'This is step description.',\n              target: 'areaOne',\n              placement: 'left',\n              xOffset: 10\n            },\n            {\n              title: 'Step Two',\n              content: 'This is step description.',\n              target: 'areaTwo',\n              placement: 'left',\n              xOffset: 15\n            }\n          ]\n        } | json}}\n        </code>\n        </pre>\n      </md-card-content>\n    </md-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\" fxFlex.sm=\"50\">\n    <md-card class=\"default\" id=\"areaTwo\">\n      <md-card-title>Initialize tour</md-card-title>\n      <md-card-content>\n        <code [style.display]=\"'block'\">\n          hopscotch.startTour(this.steps)\n        </code>\n      </md-card-content>\n    </md-card>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/app-tour/app-tour.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppTourComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_hopscotch__ = __webpack_require__("../../../../hopscotch/dist/js/hopscotch.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_hopscotch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_hopscotch__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppTourComponent = (function () {
    function AppTourComponent(snackBar) {
        this.snackBar = snackBar;
    }
    AppTourComponent.prototype.ngOnInit = function () { };
    AppTourComponent.prototype.ngOnDestroy = function () {
        __WEBPACK_IMPORTED_MODULE_2_hopscotch__["endTour"](true);
    };
    /*
    ***** Tour Steps ****
    * You can supply tourSteps directly in hopscotch.startTour instead of
    * returning value by invoking tourSteps method,
    * but DOM query methods(querySelector, getElementsByTagName etc) will not work
    */
    AppTourComponent.prototype.tourSteps = function () {
        var self = this;
        return {
            id: 'demo-tour',
            showPrevButton: true,
            onEnd: function () {
                self.snackBar.open('User tour ended!', 'close', { duration: 3000 });
            },
            onClose: function () {
                self.snackBar.open('You just closed User Tour!', 'close', { duration: 3000 });
            },
            steps: [
                {
                    title: 'Step one',
                    content: 'This is step description.',
                    target: 'areaOne',
                    placement: 'left',
                    xOffset: 10
                },
                {
                    title: 'Define your steps',
                    content: 'This is step description.',
                    target: document.querySelector('#areaOne code'),
                    placement: 'left',
                    xOffset: 0,
                    yOffset: -10
                },
                {
                    title: 'Invoke startTour function',
                    content: 'This is step description.',
                    target: document.querySelector('#areaTwo code'),
                    placement: 'left',
                    xOffset: 15,
                    yOffset: -10
                }
            ]
        };
    };
    AppTourComponent.prototype.startTour = function () {
        // Destroy running tour
        __WEBPACK_IMPORTED_MODULE_2_hopscotch__["endTour"](true);
        // Initialize new tour 
        __WEBPACK_IMPORTED_MODULE_2_hopscotch__["startTour"](this.tourSteps());
    };
    return AppTourComponent;
}());
AppTourComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-app-tour',
        template: __webpack_require__("../../../../../src/app/views/app-tour/app-tour.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/app-tour/app-tour.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["E" /* MatSnackBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_material__["E" /* MatSnackBar */]) === "function" && _a || Object])
], AppTourComponent);

var _a;
//# sourceMappingURL=app-tour.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/app-tour/app-tour.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppTourModule", function() { return AppTourModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__ = __webpack_require__("../../../flex-layout/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_tour_component__ = __webpack_require__("../../../../../src/app/views/app-tour/app-tour.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_tour_routing__ = __webpack_require__("../../../../../src/app/views/app-tour/app-tour.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppTourModule = (function () {
    function AppTourModule() {
    }
    return AppTourModule;
}());
AppTourModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["e" /* MatCardModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["c" /* MatButtonModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__["a" /* FlexLayoutModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_6__app_tour_routing__["a" /* TourRoutes */])
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_5__app_tour_component__["a" /* AppTourComponent */]]
    })
], AppTourModule);

//# sourceMappingURL=app-tour.module.js.map

/***/ }),

/***/ "../../../../../src/app/views/app-tour/app-tour.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TourRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_tour_component__ = __webpack_require__("../../../../../src/app/views/app-tour/app-tour.component.ts");

var TourRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_0__app_tour_component__["a" /* AppTourComponent */], data: { title: 'User Tour' } }
];
//# sourceMappingURL=app-tour.routing.js.map

/***/ })

});
//# sourceMappingURL=app-tour.module.chunk.js.map