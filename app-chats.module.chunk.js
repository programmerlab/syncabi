webpackJsonp(["app-chats.module"],{

/***/ "../../../../../src/app/views/app-chats/app-chats.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/app-chats/app-chats.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"p-0\">\r\n  <mat-sidenav-container class=\"chat-container\">\r\n    <!-- Left sidebar -->\r\n    <mat-sidenav class=\"chat-sidenav\" #chatSidebar [opened]=\"isSidenavOpen\" mode=\"side\">\r\n      <!-- Left side topbar -->\r\n      <mat-toolbar color=\"primary\" class=\"chat-sidebar-toolbar\">\r\n        <a href=\"\" class=\"toolbar-avatar online\">\r\n          <img src=\"assets/images/face-3.jpg\" alt=\"\">\r\n          <span class=\"status-dot\"></span>\r\n        </a>\r\n      </mat-toolbar>\r\n      <!-- Left side contact list -->\r\n      <mat-nav-list class=\"inbox-nav-list\" role=\"list\">\r\n        <mat-list-item\r\n        *ngFor=\"let user of connectedUsers\"\r\n        (click)=\"changeActiveUser(user)\">\r\n          <a \r\n          mat-list-avatar \r\n          [ngClass]=\"{online: user.isOnline}\"\r\n          class=\"toolbar-avatar\">\r\n            <img [src]=\"user.photo\" alt=\"\">\r\n            <span class=\"status-dot\"></span>\r\n          </a>\r\n          <h6 mat-line>{{user.name}}</h6>\r\n          <p mat-line class=\"text-muted\">{{user.lastMsg}}</p>\r\n        </mat-list-item>\r\n        \r\n      </mat-nav-list>\r\n    </mat-sidenav>\r\n\r\n    <!-- Right side -->\r\n    <div class=\"chats-wrap\">\r\n      <!-- Right side topbar -->\r\n      <mat-toolbar color=\"primary\" class=\"chat-toolbar mb-1\">\r\n        <!-- sidebar toggle button -->\r\n        <button \r\n        mat-icon-button\r\n        [style.alignSelf]=\"'center'\"\r\n        class=\"mr-1\" \r\n        (click)=\"chatSidebar.toggle()\"><mat-icon>short_text</mat-icon>\r\n        </button>\r\n\r\n        <!-- Selected active user -->\r\n        <div \r\n        class=\"active-chat-user\" \r\n        fxLayout=\"row\" \r\n        fxLayoutAlign=\"start center\">\r\n          <a \r\n          [ngClass]=\"{online: activeChatUser.isOnline}\" \r\n          class=\"toolbar-avatar mr-1\">\r\n            <img [src]=\"activeChatUser.photo\" alt=\"\">\r\n            <span class=\"status-dot\"></span>\r\n          </a>\r\n          <div fxLayout=\"column\">\r\n            <h6 class=\"m-0 font-normal fz-1\">{{activeChatUser.name}}</h6>\r\n          </div>\r\n        </div>\r\n\r\n        <!-- Right side top menu -->\r\n        <span fxFlex></span>\r\n        <button \r\n        [style.alignSelf]=\"'center'\" \r\n        mat-icon-button \r\n        [matMenuTriggerFor]=\"toolbarDDMenu\" \r\n        class=\"topbar-button-right hidden-on-open\">\r\n            <mat-icon>more_vert</mat-icon>\r\n        </button>\r\n        <mat-menu #toolbarDDMenu=\"matMenu\">\r\n            <button mat-menu-item><mat-icon>account_circle</mat-icon>Contact info</button>\r\n            <button mat-menu-item><mat-icon>volume_mute</mat-icon>Mute</button>\r\n            <button mat-menu-item><mat-icon>delete</mat-icon>Clear chat</button>\r\n        </mat-menu>\r\n      </mat-toolbar>\r\n\r\n      <!-- Main chat body -->\r\n      <div class=\"conversations-hold\">\r\n        <!-- single chat item -->\r\n        <div class=\"single-conversation sender\" fxLayout=\"row\">\r\n          <a href=\"\" class=\"toolbar-avatar online\">\r\n            <img src=\"assets/images/face-2.jpg\" alt=\"\">\r\n            <span class=\"status-dot\"></span>\r\n          </a>\r\n          <div>\r\n            <h5 class=\"chat-username text-muted\">Gevorg Spartak</h5>\r\n            <div class=\"conversation-msg\">\r\n              Hello, How are you\r\n            </div>\r\n            <p class=\"chat-date text-muted\">10 min ago</p>\r\n          </div>\r\n        </div>\r\n        <!-- single chat item -->\r\n        <div class=\"single-conversation me\" fxLayout=\"row\">\r\n          <a href=\"\" class=\"toolbar-avatar online\">\r\n            <img src=\"assets/images/face-3.jpg\" alt=\"\">\r\n            <span class=\"status-dot\"></span>\r\n          </a>\r\n          <div>\r\n            <h5 class=\"chat-username text-muted\">Dave Wolf</h5>\r\n            <div class=\"conversation-msg\">\r\n              I'm good, Thanks\r\n            </div>\r\n            <p class=\"chat-date text-muted\">10 min ago</p>\r\n          </div>\r\n        </div>\r\n        <!-- single chat item -->\r\n        <div class=\"single-conversation sender\" fxLayout=\"row\">\r\n          <a href=\"\" class=\"toolbar-avatar online\">\r\n            <img src=\"assets/images/face-2.jpg\" alt=\"\">\r\n            <span class=\"status-dot\"></span>\r\n          </a>\r\n          <div>\r\n            <h5 class=\"chat-username text-muted\">Gevorg Spartak</h5>\r\n            <div class=\"conversation-msg\">\r\n              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, obcaecati!\r\n            </div>\r\n            <p class=\"chat-date text-muted\">10 min ago</p>\r\n          </div>\r\n        </div>\r\n        <!-- single chat item -->\r\n        <div class=\"single-conversation me\" fxLayout=\"row\">\r\n          <a href=\"\" class=\"toolbar-avatar online\">\r\n            <img src=\"assets/images/face-3.jpg\" alt=\"\">\r\n            <span class=\"status-dot\"></span>\r\n          </a>\r\n          <div>\r\n            <h5 class=\"chat-username text-muted\">Dave Wolf</h5>\r\n            <div class=\"conversation-msg\">\r\n              Consectetur adipisicing elit. Delectus, obcaecati!\r\n            </div>\r\n            <p class=\"chat-date text-muted\">10 min ago</p>\r\n          </div>\r\n        </div>\r\n        <!-- single chat item -->\r\n        <div class=\"single-conversation sender\" fxLayout=\"row\">\r\n          <a href=\"\" class=\"toolbar-avatar online\">\r\n            <img src=\"assets/images/face-2.jpg\" alt=\"\">\r\n            <span class=\"status-dot\"></span>\r\n          </a>\r\n          <div>\r\n            <h5 class=\"chat-username text-muted\">Gevorg Spartak</h5>\r\n            <div class=\"conversation-msg\">\r\n              Consectetur adipisicing elit. Delectus, obcaecati!\r\n            </div>\r\n            <p class=\"chat-date text-muted\">10 min ago</p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <mat-divider></mat-divider>\r\n\r\n      <!-- Input box and send button -->\r\n      <div class=\"chat-input-actions\" fxLayout=\"row\">\r\n        <mat-input-container class=\"full-width mr-1\">\r\n          <input\r\n          matInput\r\n          placeholder=\"Type your message\"\r\n          value=\"\">\r\n        </mat-input-container>\r\n        <button mat-fab color=\"primary\"><mat-icon>send</mat-icon></button>\r\n      </div>\r\n\r\n    </div>\r\n  </mat-sidenav-container>\r\n</mat-card>\r\n"

/***/ }),

/***/ "../../../../../src/app/views/app-chats/app-chats.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppChatsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_flex_layout__ = __webpack_require__("../../../flex-layout/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppChatsComponent = (function () {
    function AppChatsComponent(media) {
        this.media = media;
        this.isSidenavOpen = true;
        this.activeChatUser = {
            name: 'Gevorg Spartak',
            photo: 'assets/images/face-2.jpg',
            isOnline: true,
            lastMsg: 'Hello!'
        };
        this.connectedUsers = [{
                name: 'Gevorg Spartak',
                photo: 'assets/images/face-2.jpg',
                isOnline: true,
                lastMsg: 'What\'s going!'
            }, {
                name: 'Petros Toros',
                photo: 'assets/images/face-4.jpg',
                isOnline: true,
                lastMsg: 'Send me the stories.'
            }, {
                name: 'Henrik Gevorg',
                photo: 'assets/images/face-5.jpg',
                isOnline: false,
                lastMsg: 'Great work!!'
            }, {
                name: 'Gevorg Spartak',
                photo: 'assets/images/face-6.jpg',
                isOnline: false,
                lastMsg: 'Bye'
            }, {
                name: 'Petros Toros',
                photo: 'assets/images/face-7.jpg',
                isOnline: true,
                lastMsg: 'We\'ll talk later'
            }];
    }
    AppChatsComponent.prototype.ngOnInit = function () {
        this.chatSideBarInit();
    };
    AppChatsComponent.prototype.changeActiveUser = function (user) {
        this.activeChatUser = user;
    };
    AppChatsComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNave.mode = self.isMobile ? 'over' : 'side';
        });
    };
    AppChatsComponent.prototype.chatSideBarInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    return AppChatsComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["A" /* MatSidenav */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_material__["A" /* MatSidenav */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_material__["A" /* MatSidenav */]) === "function" && _a || Object)
], AppChatsComponent.prototype, "sideNave", void 0);
AppChatsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-chats',
        template: __webpack_require__("../../../../../src/app/views/app-chats/app-chats.component.html"),
        styles: [__webpack_require__("../../../../../src/app/views/app-chats/app-chats.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_flex_layout__["b" /* ObservableMedia */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_flex_layout__["b" /* ObservableMedia */]) === "function" && _b || Object])
], AppChatsComponent);

var _a, _b;
//# sourceMappingURL=app-chats.component.js.map

/***/ }),

/***/ "../../../../../src/app/views/app-chats/app-chats.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppChatsModule", function() { return AppChatsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__ = __webpack_require__("../../../flex-layout/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_chats_component__ = __webpack_require__("../../../../../src/app/views/app-chats/app-chats.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_chats_routing__ = __webpack_require__("../../../../../src/app/views/app-chats/app-chats.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppChatsModule = (function () {
    function AppChatsModule() {
    }
    return AppChatsModule;
}());
AppChatsModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["B" /* MatSidenavModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["q" /* MatMenuModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["o" /* MatInputModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["n" /* MatIconModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["c" /* MatButtonModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["p" /* MatListModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["J" /* MatToolbarModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["e" /* MatCardModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__["a" /* FlexLayoutModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_6__app_chats_routing__["a" /* ChatsRoutes */])
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_5__app_chats_component__["a" /* AppChatsComponent */]]
    })
], AppChatsModule);

//# sourceMappingURL=app-chats.module.js.map

/***/ }),

/***/ "../../../../../src/app/views/app-chats/app-chats.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatsRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_chats_component__ = __webpack_require__("../../../../../src/app/views/app-chats/app-chats.component.ts");

var ChatsRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_0__app_chats_component__["a" /* AppChatsComponent */], data: { title: 'Chat' } }
];
//# sourceMappingURL=app-chats.routing.js.map

/***/ })

});
//# sourceMappingURL=app-chats.module.chunk.js.map